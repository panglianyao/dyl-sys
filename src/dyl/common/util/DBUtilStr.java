package dyl.common.util;


/** 
 * 检索数据库包装类.
 * <p>
 * apache dbutils handler说明：<br/>
 * ArrayHandler：把结果集中的第一行数据转成对象数组。<br/>
 * ArrayListHandler：把结果集中的每一行数据都转成一个对象数组，再存放到List中。<br/>
 * BeanHandler：将结果集中的第一行数据封装到一个对应的JavaBean实例中。<br/>
 * BeanListHandler：将结果集中的每一行数据都封装到一个对应的JavaBean实例中，存放到List里。<br/>
 * ColumnListHandler：将结果集中某一列的数据存放到List中。v
 * KeyedHandler：将结果集中的每一行数据都封装到一个Map里，然后再根据指定的key把每个Map再存放到一个Map里。<br/>
 * MapHandler：将结果集中的第一行数据封装到一个Map里，key是列名，value就是对应的值。<br/>
 * MapListHandler：将结果集中的每一行数据都封装到一个Map里，然后再存放到List。<br/>
 * ScalarHandler：将结果集中某一条记录的其中某一列的数据存成Object。<br/>
 * </p>
 * @author rocken.zeng@gmail.com
 * 
 */
public class DBUtilStr { 
	
	/**
	 * Oracle8/8i/9iO数据库(thin模式)
	 */
	public static final String DB_DRIVER_ORACLE = "oracle.jdbc.driver.OracleDriver";
	public static final int DB_KIND_ORACLE = 1;
	
	/**
	 * Sql Server7.0/2000数据库 
	 */
	public static final String DB_DRIVER_SQLSERVER = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
	public static final int DB_KIND_SQLSERVER = 2;
	
	/**
	 * DB2数据库 
	 */
	public static final String DB_DRIVER_DB2 = "com.ibm.db2.jdbc.app.DB2Driver";
	public static final int DB_KIND_DB2 = 3;
	
	/**
	 * Informix数据库 
	 */
	public static final String DB_DRIVER_INFORMIX = "com.informix.jdbc.IfxDriver";
	public static final int DB_KIND_INFORMIX = 4;
	
	/**
	 * Sybase数据库 
	 */
	public static final String DB_DRIVER_SYBASE = "com.sybase.jdbc.SybDriver";
	public static final int DB_KIND_SYBASE = 5;
	
	/**
	 * MySQL数据库 
	 */
	public static final String DB_DRIVER_MYSQL = "com.mysql.jdbc.Driver";
	public static final int DB_KIND_MYSQL = 6;
	
	/**
	 * PostgreSQL数据库 
	 */
	public static final String DB_DRIVER_POSTGRESQL = "org.postgresql.Driver";
	public static final int DB_KIND_POSTGRESQL = 7;
	

}