package dyl.common.util;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SpecialDateEditor extends PropertyEditorSupport {
	protected Log log = LogFactory.getLog(getClass());
	  @Override  
	    public void setAsText(String text) throws IllegalArgumentException {  
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	        Date date = null;  
	        try {  
	            //防止空数据出错  
	            if(StringUtils.isNotEmpty(text)){  
	                date = format.parse(text);  
	            }  
	        } catch (ParseException e) {  
	            format = new SimpleDateFormat("yyyy-MM-dd");  
	            try {  
	                date = format.parse(text);  
	            } catch (ParseException e1) {  
	                format = new SimpleDateFormat("yyyy-MM");  

	                try{  
	                    date = format.parse(text);  
	                }catch (Exception e2) {
	                	log.error("自动绑定日期数据出错", e);  
	                }  
	            }  
	        }  
	        setValue(date);  
	    }  
}
