package dyl.easycode.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

public class ExportExcel<T> {

	static Logger logger = Logger.getLogger(ExportExcel.class);

	/**
	 * 这是一个通用的方法，利用了JAVA的反射机制，可以将放置在JAVA集合中并且符号一定条件的数据以EXCEL 的形式输出到指定IO设备上
	 * 
	 * @param title
	 *            表格标题名
	 * @param headers
	 *            表格属性列名数组
	 * @param dataset
	 *            需要显示的数据集合,集合中一定要放置符合javabean风格的类的对象。此方法支持的
	 *            javabean属性的数据类型有基本数据类型及String,Date,byte[](图片数据)
	 * @param out
	 *            与输出设备关联的流对象，可以将EXCEL文档导出到本地文件或者网络中
	 * @param pattern
	 *            如果有时间数据，设定输出格式。默认为"yyy-MM-dd"
	 */
	@SuppressWarnings("unchecked")
	public HSSFWorkbook exportExcel(String title, String[] headers,Object list) {
		String pattern = "yyyy-MM-dd HH:mm:ss";
		// 声明一个工作薄
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 生成一个表格
		HSSFSheet sheet = workbook.createSheet(title);
		// 设置表格默认列宽度为15个字节
		sheet.setDefaultColumnWidth((short) 13);
		// 生成一个样式
		HSSFCellStyle style = workbook.createCellStyle();
		// 设置这些样式
		style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 生成一个字体
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.VIOLET.index);
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 把字体应用到当前的样式
		style.setFont(font);
		// 生成并设置另一个样式
		HSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		// 生成另一个字体
		HSSFFont font2 = workbook.createFont();
		font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style2.setFont(font2);

		// 声明一个画图的顶级管理器
		HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
		// 定义注释的大小和位置,详见文档
		HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,
				0, 0, 0, (short) 4, 2, (short) 6, 5));
		// 设置注释内容
		comment.setString(new HSSFRichTextString("可以在POI中添加注释！"));
		// 设置注释作者，当鼠标移动到单元格上是可以在状态栏中看到该内容.
		comment.setAuthor("leno");

		// 产生表格标题行
		HSSFRow row = sheet.createRow(0);
		for (short i = 0; i < headers.length; i++) {
			HSSFCell cell = row.createCell(i);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(headers[i].split(":")[0]);
			cell.setCellValue(text);
		}
		Collection<T> dataset = (Collection<T>)list;
		// 遍历集合数据，产生数据行
		Iterator<T> it = dataset.iterator();
		int index = 0;
		while (it.hasNext()){
			index++;
			row = sheet.createRow(index);
			Object obj = it.next();
			if(obj instanceof Map){
				Map<String, Object> map = (Map<String, Object>)obj;
				// 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
				for (short i = 0; i < headers.length; i++) {
					String key = headers[i].split(":")[1];
					HSSFCell cell = row.createCell(i);
					cell.setCellStyle(style2);
					try {
						Object value = map.get(key);
						// 判断值的类型后进行强制类型转换
						String textValue = null;
						if (value instanceof Date){
							Date date = (Date) value;
							SimpleDateFormat sdf = new SimpleDateFormat(pattern);
							textValue = sdf.format(date);
						}else {
							// 其它数据类型都当作字符串简单处理
							if(value!=null)
								textValue = value.toString();
						}
						cell.setCellValue(textValue);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else{
				for (short i = 0; i < headers.length; i++) {
					String key = headers[i].split(":")[1];
					HSSFCell cell = row.createCell(i);
					cell.setCellStyle(style2);
					try {
						StringBuilder sb = new StringBuilder(key);
						sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
						Object value = MethodUtils.invokeMethod(obj,"get"+sb);
						// 判断值的类型后进行强制类型转换
						String textValue = null;
						if (value instanceof Date){
							Date date = (Date) value;
							SimpleDateFormat sdf = new SimpleDateFormat(pattern);
							textValue = sdf.format(date);
						}else {
							// 其它数据类型都当作字符串简单处理
							if(value!=null)
								textValue = value.toString();
						}
						cell.setCellValue(textValue);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		}
		return workbook;
	}

	/** 公共的Excel导出方法 **/
	public void getCommonExcelListWay(HSSFWorkbook book, String fileName,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			book.write(outStream);
			InputStream excel = new ByteArrayInputStream(outStream.toByteArray());

			// 以流的形式下载文件。
			InputStream fis = new BufferedInputStream(excel);
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			fis.close();
			response.setCharacterEncoding("UTF-8");
			// 清空response
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Disposition", new String(("attachment;filename="+fileName).getBytes("gb2312"),"iso-8859-1"));
			response.addHeader("Content-Length", "");
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());
			response.setContentType("application/ms-excel;charset=utf-8");
			toClient.write(buffer);
			toClient.flush();
			toClient.close();
		} catch (Exception e) {
		}

	}

}