<#macro selectolums><#list table.columns as c><#if c_has_next>${c.columnName},<#else>${c.columnName}</#if></#list></#macro>
<#macro insertFlag><#list table.baseColumns as c><#if c_has_next>?,<#else>?</#if></#list></#macro>
<#macro updateColums><#list table.baseColumns as c><#if c_has_next>${c.columnName}=?,<#else>${c.columnName}=?</#if></#list></#macro>
<#macro updateVals><#list table.baseColumns as c><#if c_has_next>${table.javaProperty}.get${c.javaPropertyForGetSet}(),<#else>${table.javaProperty}.get${c.javaPropertyForGetSet}()</#if></#list></#macro>

<#list table.importList as i>
import ${i};
</#list>
import javax.annotation.Resource;

import dyl.common.util.JdbcTemplateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import dyl.sys.action.BaseAction;
import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.Page;
import javax.servlet.http.HttpServletRequest;
import dyl.sys.Annotation.Auth;
import dyl.sys.Annotation.AuthAction;
import dyl.common.bean.LayPageData;
import dyl.easycode.util.ExportExcel;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * ${sysTime}
 */
@Controller
public class ${table.className}Action extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private ${table.className}ServiceImpl ${table.javaProperty}ServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	<#if hasAuth?? && hasAuth == 'true'>
	@Auth
	</#if>
	@RequestMapping(value = "/${table.javaProperty}Main.do")
	public String main(Page page,${table.className} ${table.javaProperty},HttpServletRequest request){
		return "/{todo}/${table.javaProperty}/${table.javaProperty}Main";
	}
	
	<#if hasAuth?? && hasAuth == 'true'>
	@Auth
	</#if>
	@ResponseBody
	@RequestMapping(value = "/find${table.className}List.do")
	public LayPageData  find${table.className}List(Page page,${table.className} ${table.javaProperty},HttpServletRequest request){
		try {
			return getlayPageData(${table.javaProperty}ServiceImpl.find${table.className}List(page, ${table.javaProperty}),page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	<#if hasEdit?? && hasEdit == 'true'>
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	<#if hasAuth?? && hasAuth == 'true'>
	@Auth
	</#if>
	@RequestMapping(value = "/${table.javaProperty}Form.do")
	public String  ${table.javaProperty}Form(${table.primaryKey.javaType} ${table.primaryKey.javaProperty},HttpServletRequest request){
		try {
			if(${table.primaryKey.javaProperty}!=null)request.setAttribute("${table.javaProperty}",${table.javaProperty}ServiceImpl.get${table.className}(${table.primaryKey.javaProperty}));
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/{todo}/${table.javaProperty}/${table.javaProperty}Form";
	}
	/**
	 * 说明：插入${table.tableName}
	 * @return int >0代表操作成功
	 */
	<#if hasAuth?? && hasAuth == 'true'>
	@Auth(action=AuthAction.ADD)
	</#if>
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}Add.do")
	public R add(${table.className} ${table.javaProperty},HttpServletRequest request){
		try {
			//${table.javaProperty}.setCreator(getSysUser(request).getId());
			int ret = ${table.javaProperty}ServiceImpl.insert${table.className}(${table.javaProperty});
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新${table.tableName}
	 * @return int >0代表操作成功
	 */
	<#if hasAuth?? && hasAuth == 'true'>
	@Auth(action=AuthAction.UPDATE)
	</#if>
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}Update.do")
	public R update(${table.className} ${table.javaProperty},HttpServletRequest request){
		try {
			int ret = ${table.javaProperty}ServiceImpl.update${table.className}(${table.javaProperty});
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	</#if>
	<#if hasDelete?? && hasDelete == 'true'>
	/**
	 * 说明：根据主键删除表${table.tableName}中的记录
	 * @return int >0代表操作成功
	 */
	<#if hasAuth?? && hasAuth == 'true'>
	@Auth(action=AuthAction.DELETE)
	</#if>
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}Delete.do")
	public R delete${table.className}(String dataIds,HttpServletRequest request){
		try {
			int[] ret = ${table.javaProperty}ServiceImpl.delete${table.className}(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	</#if>
	<#if hasExport?? && hasExport == 'true'>
	@RequestMapping(value = "/${table.javaProperty}Export.do")
	public void wgldqsExport(${table.className} ${table.javaProperty},HttpServletRequest request,HttpServletResponse response){
		try {
			String fileName = "导出";
			String[] headers = new String[]{<#list table.baseColumns as c>"${c.remarks!c.javaProperty}:${c.javaProperty}"<#if c_has_next>,<#else></#if></#list>};
			List<${table.className}> countlist = ${table.javaProperty}ServiceImpl.find${table.className}List(null, ${table.javaProperty});
			ExportExcel<Map<String,Object>> ex = new ExportExcel<Map<String,Object>>();
			ex.getCommonExcelListWay(ex.exportExcel("sheet1",headers,countlist),fileName+".xls", request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	</#if>
}
