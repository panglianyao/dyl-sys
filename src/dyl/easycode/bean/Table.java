/*
 * dyl
 */
package dyl.easycode.bean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dyl.easycode.util.StringUtil;



public class Table implements java.io.Serializable {

    private static final long serialVersionUID = -7246043091254837124L;
    private String            tableName;
    private String            tableType;
    private String            tableAlias;
    private String            remarks;
    private String            className;	//JAVA属性名称 大写开头
    private String            javaProperty; //JAVA属性名称 小写开头
    private String            catalog          = null;
    private String            schema           = null;
    private List<Column>      baseColumns      = new ArrayList<Column>();
    private List<Column>      primaryKeys      = new ArrayList<Column>();
    private Set<String>       importList       = new HashSet<String>();
    private String 			  packageName ;
    private Column            primaryKey;

    public Table(){
    }
    public Table(String tableName){
    	this.setTableName(tableName);
    }
    public String getTableName() {
        return tableName;
    }
    public void setPrimaryKey(Column primaryKey) {
		this.primaryKey = primaryKey;
	}
    public Column getPrimaryKey() {
		return primaryKey;
	} 
    public void setTableName(String tableName) {
        this.tableName = tableName;
        if(tableName.startsWith("w_base_")||tableName.startsWith("W_BASE_")){
        	tableAlias=tableName.substring(7,tableName.length());
        }else{
        	if(tableName.startsWith("w_")||tableName.startsWith("W_")
        			||tableName.startsWith("t_")||tableName.startsWith("V_")||tableName.startsWith("v_")||tableName.startsWith("T_")||tableName.startsWith("T_")||tableName.startsWith("h_")){
        		tableAlias=tableName.substring(2,tableName.length());
        	}else{
        		this.tableAlias = tableName;
        	}
        }
        this.className = StringUtil.getCamelCaseString(tableAlias, true);
        this.javaProperty = StringUtil.getCamelCaseString(tableAlias, false);
    }

    public String getRemarks() {
        return remarks == null ? "" : remarks.trim();
    }

    public boolean isHasRemarks() {
        return StringUtil.isNotEmpty(remarks);
    }

    public String getRemarksUnicode() {
        return StringUtil.toUnicodeString(getRemarks());
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Column getColumn(String columnName) {
        for (Column column : primaryKeys) {
            if (column.getColumnName().equals(columnName)) {
                return column;
            }
        }
        for (Column column : baseColumns) {
            if (column.getColumnName().equals(columnName)) {
                return column;
            }
        }
        return null;
    }

    public List<Column> getColumns() {
        List<Column> allColumns = new ArrayList<Column>();
        allColumns.addAll(primaryKeys);
        allColumns.addAll(baseColumns);
        return allColumns;
    }
    
    public List<Column> getBaseColumns() {
        return baseColumns;
    }

    public void addBaseColumn(Column column) {
        this.baseColumns.add(column);
    }

    public List<Column> getPrimaryKeys() {
        return primaryKeys;
    }

    public void addPrimaryKey(Column primaryKeyColumn) {
        this.primaryKeys.add(primaryKeyColumn);
    }

    public String getJavaProperty() {
        return javaProperty;
    }

    public void setJavaProperty(String javaProperty) {
        this.javaProperty = javaProperty;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
        this.className = StringUtil.getCamelCaseString(tableAlias, true);
        this.javaProperty = StringUtil.getCamelCaseString(tableAlias, false);
    }

    public boolean isHasDateColumn() {
        for (Column column : getColumns()) {
            if (column.isDate()) {
                return true;
            }
        }
        return false;
    }

    public boolean isHasBigDecimalColumn() {
        for (Column column : getColumns()) {
            if (column.isBigDecimal()) {
                return true;
            }
        }
        return false;
    }

    public boolean isHasNotNullColumn() {
        for (Column column : getColumns()) {
            if (!column.getNullable() && !column.isString()) {
                return true;
            }
        }
        return false;
    }

    public boolean isHasNotBlankColumn() {
        for (Column column : getColumns()) {
            if (!column.getNullable() && column.isString()) {
                return true;
            }
        }
        return false;
    }


    public void setBaseColumns(List<Column> baseColumns) {
		this.baseColumns = baseColumns;
	}
	public Set<String> getImportList() {
		return importList;
	}
	public void setImportList(Set<String> importList) {
		this.importList = importList;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
}
