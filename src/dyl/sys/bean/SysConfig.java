package dyl.sys.bean;

/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-11-09 20:03:27
*/

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;

public class SysConfig implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:是
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：键
	*对应db字段名:c_key 类型:VARCHAR2(64)  
	*是否可以为空:是
	*/
	
	private  String  cKey;
	
	/*字段说明：值
	*对应db字段名:c_value 类型:VARCHAR2(64)  
	*是否可以为空:是
	*/
	
	private  String  cValue;
	
	/*字段说明：说明
	*对应db字段名:note 类型:VARCHAR2(128)  
	*是否可以为空:是
	*/
	
	private  String  note;
	
	/*字段说明：排序
	*对应db字段名:px 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  px;
	
	/*字段说明：是否可以修改
	*对应db字段名:can_edit 类型:CHAR(1)  
	*是否可以为空:是
	*/
	
	private  String  canEdit;
	
	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public String getCKey() {
        return cKey;
    }
    public void setCKey(String cKey) {
        this.cKey = cKey;
    }
    public String getCValue() {
        return cValue;
    }
    public void setCValue(String cValue) {
        this.cValue = cValue;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public BigDecimal getPx() {
        return px;
    }
    public void setPx(BigDecimal px) {
        this.px = px;
    }
    public String getCanEdit() {
        return canEdit;
    }
    public void setCanEdit(String canEdit) {
        this.canEdit = canEdit;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"cKey:"+cKey+"\n"+
		
		"cValue:"+cValue+"\n"+
		
		"note:"+note+"\n"+
		
		"px:"+px+"\n"+
		
		"canEdit:"+canEdit+"\n"+
		"";
	}
}
