package dyl.sys.bean;

/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-11-09 20:06:54
*/

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;
import java.util.Date;

public class SysConfigInfo implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:是
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：
	*对应db字段名:type 类型:VARCHAR2(256)  
	*是否可以为空:是
	*/
	
	private  String  type;
	
	/*字段说明：键
	*对应db字段名:c_key 类型:VARCHAR2(256)  
	*是否可以为空:是
	*/
	
	private  String  cKey;
	
	/*字段说明：值
	*对应db字段名:c_value 类型:VARCHAR2(256)  
	*是否可以为空:是
	*/
	
	private  String  cValue;
	
	/*字段说明：
	*对应db字段名:create_time 类型:DATE(7)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createTime;
	
	/*字段说明：说明
	*对应db字段名:note 类型:VARCHAR2(256)  
	*是否可以为空:是
	*/
	
	private  String  note;
	
	/*字段说明：
	*对应db字段名:px 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  px;
	
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getCKey() {
        return cKey;
    }
    public void setCKey(String cKey) {
        this.cKey = cKey;
    }
    public String getCValue() {
        return cValue;
    }
    public void setCValue(String cValue) {
        this.cValue = cValue;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public BigDecimal getPx() {
        return px;
    }
    public void setPx(BigDecimal px) {
        this.px = px;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"type:"+type+"\n"+
		
		"cKey:"+cKey+"\n"+
		
		"cValue:"+cValue+"\n"+
		
		"createTime:"+createTime+"\n"+
		
		"note:"+note+"\n"+
		
		"px:"+px+"\n"+
		"";
	}
}
