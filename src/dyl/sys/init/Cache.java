package dyl.sys.init;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import dyl.common.util.JdbcTemplateUtil;
import dyl.sys.bean.SysConfigInfo;
@Service
public class Cache {
	public static final Log log = LogFactory.getLog(Cache.class);
	@Resource
	public JdbcTemplateUtil jdbcTemplate;
	@PostConstruct
	public void load() throws SQLException{
		//清空
		selectDataMap.clear();
		sysConfigMap.clear();
		configMap.clear();
		
		log.info("缓存下拉框数据。。");
		loadSelectData();
		log.info("开始缓存系统配置表。。");
		loadSysConfig();
	}
	public static Map<String,List<Map<String,Object>>>  selectDataMap = new HashMap<String, List<Map<String,Object>>>();
	public final static Map<String, String> sqlMap = new HashMap<String, String>() {
		{  
			
		}  
	};
	public void loadSelectData(){
		for(Entry<String,String> entry:sqlMap.entrySet()){
			List<Object> para = new ArrayList<Object>();
			String sql = entry.getValue();
			List<Map<String,Object>> list = jdbcTemplate.queryForList(sql,para.toArray());
			selectDataMap.put(entry.getKey(),list);
		}
	}
	//系统配置表
	public static Map<String,Map<String,String>> sysConfigMap = new HashMap<String,Map<String,String>>();
	public static Map<String,String> configMap = new HashMap<String,String>();
	public void loadSysConfig(){
		//sysConfigMap.clear();
		String sql = "select type,c_key,c_value from SYS_CONFIG_INFO order by type,px";
		List<SysConfigInfo>  sysConfigList =  jdbcTemplate.queryForListBean(sql, SysConfigInfo.class);
		for (SysConfigInfo s:sysConfigList){
			if(sysConfigMap.get(s.getType())!=null){
				sysConfigMap.get(s.getType()).put(s.getCKey(),s.getCValue());
			}else{
				Map<String,String> map  = new LinkedHashMap<String, String>();
				map.put(s.getCKey(),s.getCValue());
				sysConfigMap.put(s.getType(),map);
			}
			if(StringUtils.isEmpty(s.getType()))configMap.put(s.getCKey(), s.getCValue());
		}
		for(Entry<String, List<Map<String,Object>>> entry:selectDataMap.entrySet()){
			for (int i = 0; i < entry.getValue().size(); i++) {
				 int num=0;
				 String key = "";
				 String value = "";
				 for (Entry<String,Object> map : entry.getValue().get(i).entrySet()){
					 if(num==0){
						 key=String.valueOf(map.getValue());
						 num++;
					 }else if(num++==1){
						 value=(String)map.getValue(); 
					 }
			     }
				 if(sysConfigMap.get(entry.getKey())!=null){
					 sysConfigMap.get(entry.getKey()).put(key, value);
				 }else{
					 Map<String,String> map_ = new LinkedHashMap<String, String>();
					 map_.put(key, value);
					 sysConfigMap.put(entry.getKey(),map_);
				 }
			}
		}
	}
	public void loadSysConfigByType(String type){
		sysConfigMap.remove(type);
		if(selectDataMap.get(type)!=null){
			selectDataMap.remove(type);
			List<Map<String,Object>> list = jdbcTemplate.queryForList(sqlMap.get(type));
			selectDataMap.put(type,list);
			for (int i = 0; i < selectDataMap.get(type).size(); i++) {
				 int num=0;
				 String key = "";
				 String value = "";
				 for (Entry<String,Object> map : selectDataMap.get(type).get(i).entrySet()){
					 if(num==0){
						 key=String.valueOf(map.getValue());
						 num++;
					 }else if(num++==1){
						 value=(String)map.getValue(); 
					 }
			     }
				 if(sysConfigMap.get(type)!=null){
					 sysConfigMap.get(type).put(key, value);
				 }else{
					 Map<String,String> map_ = new LinkedHashMap<String, String>();
					 map_.put(key, value);
					 sysConfigMap.put(type,map_);
				 }
			}
		}else{
			String sql = "select type,c_key,c_value from SYS_CONFIG_INFO where type = ? order by type,px";
			List<SysConfigInfo>  sysConfigList =  jdbcTemplate.queryForListBean(sql,new Object[]{type},SysConfigInfo.class);
			for (SysConfigInfo s:sysConfigList){
				if(sysConfigMap.get(s.getType())!=null){
					sysConfigMap.get(s.getType()).put(s.getCKey(),s.getCValue());
				}else{
					Map<String,String> map  = new LinkedHashMap<String, String>();
					map.put(s.getCKey(),s.getCValue());
					sysConfigMap.put(s.getType(),map);
				}
				if(StringUtils.isEmpty(s.getType()))configMap.put(s.getCKey(), s.getCValue());
			}
		}
	
	}
}
