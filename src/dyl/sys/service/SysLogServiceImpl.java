package dyl.sys.service;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysLog;
/**

 * @author Dyl
 * 2017-06-05 17:33:32
 */
@Service
@Transactional
public class SysLogServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表SYS_LOG记录封装成List集合
	 * @return List<SysLog
	 */
	public List<SysLog>  findSysLogList(Page page,SysLog sysLog) throws Exception{
		String sql = "select id,url,para,loginuser,ip,time from SYS_LOG t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(StringUtils.isNotEmpty(sysLog.getUrl())){
			sql+=" and t.url like ?";
			con.add("%"+sysLog.getUrl()+"%");
		}
		if(sysLog.getLoginuser()!=null){
			sql+=" and t.loginuser=?";
			con.add(sysLog.getLoginuser());
		}
		if(StringUtils.isNotEmpty(sysLog.getIp())){
			sql+=" and t.ip like ?";
			con.add("%"+sysLog.getIp()+"%");
		}
		sql+=" order by time desc";
		List<SysLog> sysLogList =  jdbcTemplate.queryForListBeanByPage(sql, con, SysLog.class, page);
		
		return sysLogList;
	}
}
