package dyl.sys.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.sys.bean.SysConfigInfo;
/**

 * @author Dyl
 * 2017-11-09 20:06:54
 */
@Service
@Transactional
public class SysConfigInfoServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表SYS_CONFIG_INFO记录封装成List集合
	 * @return List<SysConfigInfo
	 */
	public List<SysConfigInfo>  findSysConfigInfoList(SysConfigInfo sysConfigInfo) throws Exception{
		//String sql = "select id,type,c_key,c_value,create_time,note,px from SYS_CONFIG_INFO t where 1=1";
		String sql = "select * from SYS_CONFIG_INFO t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(sysConfigInfo.getId()!=null){
			sql+=" and t.id=?";
			con.add(sysConfigInfo.getId());
		}
		if(StringUtils.isNotEmpty(sysConfigInfo.getType())){
			sql+=" and t.type like ?";
			con.add("%"+sysConfigInfo.getType()+"%");
		}
		if(StringUtils.isNotEmpty(sysConfigInfo.getCKey())){
			sql+=" and t.c_key like ?";
			con.add("%"+sysConfigInfo.getCKey()+"%");
		}
		if(StringUtils.isNotEmpty(sysConfigInfo.getCValue())){
			sql+=" and t.c_value like ?";
			con.add("%"+sysConfigInfo.getCValue()+"%");
		}
		if(sysConfigInfo.getCreateTime()!=null){
			sql+=" and t.create_time=?";
			con.add(sysConfigInfo.getCreateTime());
		}
		if(StringUtils.isNotEmpty(sysConfigInfo.getNote())){
			sql+=" and t.note like ?";
			con.add("%"+sysConfigInfo.getNote()+"%");
		}
		if(sysConfigInfo.getPx()!=null){
			sql+=" and t.px=?";
			con.add(sysConfigInfo.getPx());
		}
		List<SysConfigInfo> sysConfigInfoList =  jdbcTemplate.queryForListBean(sql, con.toArray(), SysConfigInfo.class);
		
		return sysConfigInfoList;
	}
	/**
	 * 说明：根据主键查询表SYS_CONFIG_INFO中的一条记录 
	 * @return SysConfigInfo
	 */
	public SysConfigInfo getSysConfigInfo(BigDecimal  id) throws Exception{
		//String sql = "select id,type,c_key,c_value,create_time,note,px from SYS_CONFIG_INFO where id=?";
		String sql = "select * from SYS_CONFIG_INFO where id=?";
		SysConfigInfo sysConfigInfo  =  jdbcTemplate.queryForBean(sql, new Object[]{id},SysConfigInfo.class); 
		return sysConfigInfo;
	}
	/**
	 * 说明：往表SYS_CONFIG_INFO中插入一条记录
	 * @return int >0代表操作成功
	 */
	public int insertSysConfigInfo(SysConfigInfo sysConfigInfo) throws Exception{
		String sql = "insert into SYS_CONFIG_INFO(id,type,c_key,c_value,create_time,note,px) values("+DylSqlUtil.getnextSeqNextVal()+",?,?,?,?,?,?)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{sysConfigInfo.getType(),sysConfigInfo.getCKey(),sysConfigInfo.getCValue(),sysConfigInfo.getCreateTime(),sysConfigInfo.getNote(),sysConfigInfo.getPx()});
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表SYS_CONFIG_INFO中的记录
	 * @return int >0代表操作成功
	 */
	public int updateSysConfigInfo(SysConfigInfo sysConfigInfo) throws Exception{
		String sql = "update SYS_CONFIG_INFO t set ";
		List<Object> con = new ArrayList<Object>();
		if(sysConfigInfo.getType()!=null){
			sql+="t.type=?,";
			con.add(sysConfigInfo.getType());
		}
		if(sysConfigInfo.getCKey()!=null){
			sql+="t.c_key=?,";
			con.add(sysConfigInfo.getCKey());
		}
		if(sysConfigInfo.getCValue()!=null){
			sql+="t.c_value=?,";
			con.add(sysConfigInfo.getCValue());
		}
		if(sysConfigInfo.getCreateTime()!=null){
			sql+="t.create_time=?,";
			con.add(sysConfigInfo.getCreateTime());
		}
		if(sysConfigInfo.getNote()!=null){
			sql+="t.note=?,";
			con.add(sysConfigInfo.getNote());
		}
		if(sysConfigInfo.getPx()!=null){
			sql+="t.px=?,";
			con.add(sysConfigInfo.getPx());
		}
		sql=sql.substring(0,sql.length()-1);
		sql+=" where id=?";
		con.add(sysConfigInfo.getId());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		return returnVal;
	}
	/**
	 * 说明：根据主键删除表SYS_CONFIG_INFO中的记录
	 * @return int >0代表操作成功
	 */
	public int[] deleteSysConfigInfo(String  ids) throws Exception{
		String sql = "delete from  SYS_CONFIG_INFO where id=?";
		String[] idArr  =  ids.split(",");
		List<Object[]> paraList = new ArrayList<Object[]>(); 
		for (int i = 0; i < idArr.length; i++) {
			paraList.add(new Object[]{idArr[i]});
		}
		return jdbcTemplate.batchUpdate(sql,paraList);
	}
}
