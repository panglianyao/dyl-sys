package dyl.sys.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysQuartz;
/**

 * @author Dyl
 * 2017-05-25 16:04:28
 */
@Service
@Transactional
public class SysQuartzServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表SYS_QUARTZ记录封装成List集合
	 * @return List<SysQuartz
	 */
	public List<SysQuartz>  findSysQuartzList(Page page,SysQuartz sysQuartz) throws Exception{
		String sql = "select id,triggername,cronexpression,jobdetailname,targetobject,methodname,concurrent,state,create_date from SYS_QUARTZ t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(sysQuartz.getId()!=null){
			sql+=" and t.id=?";
			con.add(sysQuartz.getId());
		}
		if(StringUtils.isNotEmpty(sysQuartz.getTriggername())){
			sql+=" and t.triggername like ?";
			con.add("%"+sysQuartz.getTriggername()+"%");
		}
		if(StringUtils.isNotEmpty(sysQuartz.getCronexpression())){
			sql+=" and t.cronexpression like ?";
			con.add("%"+sysQuartz.getCronexpression()+"%");
		}
		if(StringUtils.isNotEmpty(sysQuartz.getJobdetailname())){
			sql+=" and t.jobdetailname like ?";
			con.add("%"+sysQuartz.getJobdetailname()+"%");
		}
		if(StringUtils.isNotEmpty(sysQuartz.getTargetobject())){
			sql+=" and t.targetobject like ?";
			con.add("%"+sysQuartz.getTargetobject()+"%");
		}
		if(StringUtils.isNotEmpty(sysQuartz.getMethodname())){
			sql+=" and t.methodname like ?";
			con.add("%"+sysQuartz.getMethodname()+"%");
		}
		if(sysQuartz.getConcurrent()!=null){
			sql+=" and t.concurrent=?";
			con.add(sysQuartz.getConcurrent());
		}
		if(sysQuartz.getState()!=null){
			sql+=" and t.state=?";
			con.add(sysQuartz.getState());
		}
		if(sysQuartz.getCreateDate()!=null){
			sql+=" and t.create_date=?";
			con.add(sysQuartz.getCreateDate());
		}
		List<SysQuartz> sysQuartzList =  jdbcTemplate.queryForListBeanByPage(sql, con, SysQuartz.class, page);
		
		return sysQuartzList;
	}
	/**
	 * 说明：根据主键查询表SYS_QUARTZ中的一条记录 
	 * @return SysQuartz
	 */
	public SysQuartz getSysQuartz(BigDecimal  id) throws Exception{
		String sql = "select * from SYS_QUARTZ where id=?";
		SysQuartz sysQuartz  =  jdbcTemplate.queryForBean(sql, new Object[]{id},SysQuartz.class); 
		return sysQuartz;
	}
	/**
	 * 说明：往表SYS_QUARTZ中插入一条记录
	 * @return int >0代表操作成功
	 */
	public int insertSysQuartz(SysQuartz sysQuartz) throws Exception{
		String sql = "insert into SYS_QUARTZ(id,triggername,cronexpression,jobdetailname,targetobject,methodname,concurrent,state) values("+DylSqlUtil.getnextSeqNextVal()+",?,?,?,?,?,?,?)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{sysQuartz.getTriggername(),sysQuartz.getCronexpression(),sysQuartz.getJobdetailname(),sysQuartz.getTargetobject(),sysQuartz.getMethodname(),sysQuartz.getConcurrent(),sysQuartz.getState()});
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表SYS_QUARTZ中的记录
	 * @return int >0代表操作成功
	 */
	public int updateSysQuartz(SysQuartz sysQuartz) throws Exception{
		String sql = "update SYS_QUARTZ t set ";
		List<Object> con = new ArrayList<Object>();
		if(sysQuartz.getTriggername()!=null){
			sql+="t.triggername=?,";
			con.add(sysQuartz.getTriggername());
		}
		if(sysQuartz.getCronexpression()!=null){
			sql+="t.cronexpression=?,";
			con.add(sysQuartz.getCronexpression());
		}
		if(sysQuartz.getJobdetailname()!=null){
			sql+="t.jobdetailname=?,";
			con.add(sysQuartz.getJobdetailname());
		}
		if(sysQuartz.getTargetobject()!=null){
			sql+="t.targetobject=?,";
			con.add(sysQuartz.getTargetobject());
		}
		if(sysQuartz.getMethodname()!=null){
			sql+="t.methodname=?,";
			con.add(sysQuartz.getMethodname());
		}
		if(sysQuartz.getConcurrent()!=null){
			sql+="t.concurrent=?,";
			con.add(sysQuartz.getConcurrent());
		}
		if(sysQuartz.getState()!=null){
			sql+="t.state=?,";
			con.add(sysQuartz.getState());
		}
		sql=sql.substring(0,sql.length()-1);
		sql+=" where id=?";
		con.add(sysQuartz.getId());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		return returnVal;
	}
	/**
	 * 说明：根据主键删除表SYS_QUARTZ中的记录
	 * @return int >0代表操作成功
	 */
	public int[] deleteSysQuartz(String  ids) throws Exception{
		String sql = "delete from  SYS_QUARTZ where id=?";
		String[] idArr  =  ids.split(",");
		List<Object[]> paraList = new ArrayList<Object[]>(); 
		for (int i = 0; i < idArr.length; i++) {
			paraList.add(new Object[]{idArr[i]});
		}
		return jdbcTemplate.batchUpdate(sql,paraList);
	}
}
