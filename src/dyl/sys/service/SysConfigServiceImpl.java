package dyl.sys.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysConfig;
/**

 * @author Dyl
 * 2017-11-09 20:03:27
 */
@Service
@Transactional
public class SysConfigServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表SYS_CONFIG记录封装成List集合
	 * @return List<SysConfig
	 */
	public List<SysConfig>  findSysConfigList(Page page,SysConfig sysConfig) throws Exception{
		//String sql = "select id,c_key,c_value,note,px,can_edit from SYS_CONFIG t where 1=1";
		String sql = "select * from SYS_CONFIG t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(sysConfig.getId()!=null){
			sql+=" and t.id=?";
			con.add(sysConfig.getId());
		}
		if(StringUtils.isNotEmpty(sysConfig.getCKey())){
			sql+=" and t.c_key like ?";
			con.add("%"+sysConfig.getCKey()+"%");
		}
		if(StringUtils.isNotEmpty(sysConfig.getCValue())){
			sql+=" and t.c_value like ?";
			con.add("%"+sysConfig.getCValue()+"%");
		}
		if(StringUtils.isNotEmpty(sysConfig.getType())){
			sql+=" and t.type = ?";
			con.add(sysConfig.getType());
		}else{
			sql+=" and t.type  is null ";
		}
		List<SysConfig> sysConfigList =  jdbcTemplate.queryForListBeanByPage(sql, con, SysConfig.class, page);
		
		return sysConfigList;
	}
	/**
	 * 说明：根据主键查询表SYS_CONFIG中的一条记录 
	 * @return SysConfig
	 */
	public SysConfig getSysConfig(BigDecimal  id) throws Exception{
		//String sql = "select id,c_key,c_value,note,px,can_edit from SYS_CONFIG where id=?";
		String sql = "select * from SYS_CONFIG where id=?";
		SysConfig sysConfig  =  jdbcTemplate.queryForBean(sql, new Object[]{id},SysConfig.class); 
		return sysConfig;
	}
	/**
	 * 说明：往表SYS_CONFIG中插入一条记录
	 * @return int >0代表操作成功
	 */
	public int insertSysConfig(SysConfig sysConfig) throws Exception{
		String sql = "insert into SYS_CONFIG(id,c_key,c_value,note,px,can_edit) values("+DylSqlUtil.getnextSeqNextVal()+",?,?,?,?,?)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{sysConfig.getCKey(),sysConfig.getCValue(),sysConfig.getNote(),sysConfig.getPx(),sysConfig.getCanEdit()});
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表SYS_CONFIG中的记录
	 * @return int >0代表操作成功
	 */
	public int updateSysConfig(SysConfig sysConfig) throws Exception{
		String sql = "update SYS_CONFIG t set ";
		List<Object> con = new ArrayList<Object>();
		if(sysConfig.getCKey()!=null){
			sql+="t.c_key=?,";
			con.add(sysConfig.getCKey());
		}
		if(sysConfig.getCValue()!=null){
			sql+="t.c_value=?,";
			con.add(sysConfig.getCValue());
		}
		if(sysConfig.getNote()!=null){
			sql+="t.note=?,";
			con.add(sysConfig.getNote());
		}
		if(sysConfig.getPx()!=null){
			sql+="t.px=?,";
			con.add(sysConfig.getPx());
		}
		if(sysConfig.getCanEdit()!=null){
			sql+="t.can_edit=?,";
			con.add(sysConfig.getCanEdit());
		}
		sql=sql.substring(0,sql.length()-1);
		sql+=" where id=?";
		con.add(sysConfig.getId());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		return returnVal;
	}
}
