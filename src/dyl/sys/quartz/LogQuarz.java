package dyl.sys.quartz;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import dyl.common.util.ConfigUtil;
import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.sys.bean.SysLog;
import dyl.sys.service.SysLogServiceImpl;
@Service
public class LogQuarz {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Autowired
	private SysLogServiceImpl sysLogServiceImpl;
	public void execute() throws Exception{
		 String filePath = ConfigUtil.getValue("sysLogPath");
		 BufferedReader reader = new BufferedReader(new FileReader(filePath));
         String line = null;
         // 一行一行的读
         String sql = "insert into SYS_LOG(id,url,para,loginuser,ip,time) values("+DylSqlUtil.getnextSeqNextVal()+",?,?,?,?,?)";
         List<Object[]> list = new ArrayList<Object[]>();
         while ((line = reader.readLine()) != null) {
	       	 SysLog sysLog = JSONObject.parseObject(line,SysLog.class);
	       	 list.add(new Object[]{sysLog.getUrl(),sysLog.getPara(),sysLog.getLoginuser(),sysLog.getIp(),sysLog.getTime()});
	       	 if(list.size()>0&&list.size()%100==0){//100条一起提交
	       		jdbcTemplate.batchUpdate(sql, list);
	       		list.clear();
	       	 }
         }
         jdbcTemplate.batchUpdate(sql, list);//提交剩余的
         //删除文件中的数据
         FileOutputStream out = new FileOutputStream(filePath,false); 
		 out.write(new String("").getBytes()); 
		 out.close();
         reader.close();
     } 
}
