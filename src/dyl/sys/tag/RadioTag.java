package dyl.sys.tag;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;

import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.SpringContextUtil;
import dyl.sys.init.Cache;

/**
 * <DL>
 * <DT><B> 类名称: </B>选择列表初始化标签 </DT><br>
 * <p>
 * </DL>
 * <p>
 * @author Dyl
 * @version 1.00, 2012-7-26
 */
public class RadioTag extends TagSupport {
	private static final long serialVersionUID = 8125593281113591128L;

	private String name;
	public void setName(String name){
		this.name = name;
	}
	private String layFilter;
	public void setLayFilter(String layFilter) {
		this.layFilter = layFilter;
	}
	private String selectId;
	public void setSelectId(String selectId) {
		this.selectId = selectId;
	}
	private String configType;
	public void setConfigType(String configType) {
		this.configType = configType;
	}
	private Boolean isLastSelect = false;
	public void setIsLastSelect(Boolean isLastSelect) {
		this.isLastSelect = isLastSelect;
	}
	JdbcTemplateUtil jdbcTemplate  =  (JdbcTemplateUtil)SpringContextUtil.getApplicationContext().getBean("jdbcTemplate");
	public int doStartTag() throws JspException {
		Writer out = pageContext.getOut();
		String str = "";
		try{
			if(StringUtils.isNotEmpty(layFilter)){
				str+=" lay-filter='"+layFilter+"' ";
			}
			//List<SysConfigInfo>  configList =  AuthCache.sysConfigMap.get(configType);
			Map<String,String>  configMap =  Cache.sysConfigMap.get(configType);
			/*if(configMap!=null){
				for(Entry<String,String> entry:configMap.entrySet()){
					str+=setOption(entry.getKey(), entry.getValue());
				}
			}*/
			/*int s = !isLastSelect?0:configList.size()-1;
			for (int i= 0;i<configList.size();i++) {
				SysConfigInfo c = configList.get(i);
				 str+=" <input type='radio' name='"+name+"' value='"+c.getCKey()+"' title='"+c.getCValue()+"'"
						 +((StringUtils.isEmpty(selectId) && s==i)||c.getCKey().equals(selectId)?"checked='checked'":"")+">";
			}*/
			out.write(str.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
}
