package dyl.sys.tag;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;

import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.SpringContextUtil;
import dyl.sys.init.Cache;

/**
 * <DL>
 * <DT><B> 类名称: </B>选择列表初始化标签 </DT><br>
 * <p>
 * </DL>
 * <p>
 * @author Dyl
 * @version 1.00, 2012-7-26
 */
public class SelectTag extends TagSupport {
	private static final long serialVersionUID = 8125593281113591128L;

	private String name;
	public void setName(String name){
		this.name = name;
	}
	private String layFilter;
	public void setLayFilter(String layFilter) {
		this.layFilter = layFilter;
	}
	private String selectId;
	public void setSelectId(String selectId) {
		this.selectId = selectId;
	}
	private String configType;
	public void setConfigType(String configType) {
		this.configType = configType;
	}
	private Boolean isRequired =false;
	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}
	private Boolean isCache = true ;
	public void setIsCache(Boolean isCache) {
		this.isCache = isCache;
	}
	private String yxdm;
	public void setYxdm(String yxdm) {
		this.yxdm = yxdm;
	}
	private Boolean hasEmptyOp = true ;
	public void setHasEmptyOp(Boolean hasEmptyOp) {
		this.hasEmptyOp = hasEmptyOp;
	}
	private String title;
	public void setTitle(String title) {
		this.title = title;
	}
	JdbcTemplateUtil jdbcTemplate  =  (JdbcTemplateUtil)SpringContextUtil.getApplicationContext().getBean("jdbcTemplate");
	public int doStartTag() throws JspException {
		Writer out = pageContext.getOut();
		String str = "<select  class='layui-select' ";
		try{
			if(isRequired){
				str+="lay-verify='required' ";
			}
			if(StringUtils.isNotEmpty(name)){
				str+=" name='"+name+"' ";
			}
			if(StringUtils.isNotEmpty(layFilter)){
				str+=" lay-filter='"+layFilter+"' ";
			}
			str+=" >";
			if(hasEmptyOp){
				if(StringUtils.isEmpty(title))
					str+="<option value=''>全部</option>";
				else{
					str+="<option value=''>"+title+"</option>";
				}
			}
			
			Map<String,String>  configMap =  Cache.sysConfigMap.get(configType);
			if(configMap!=null){
				for(Entry<String,String> entry:configMap.entrySet()){
					str+=setOption(entry.getKey(), entry.getValue());
				}
			}
			str+="</select>";
			out.write(str.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
	private String setOption(String key,String value){
		String str = "";
		if(StringUtils.isNotEmpty(selectId)&&selectId.equals(key)){
			 str+="<option value='"+key+"' selected='selected' >"+value+"</option>";
		 }else{
			 str+="<option value='"+key+"'>"+value+"</option>";
		 }
		return str ;
	}
}
