package dyl.sys.tag;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;

import dyl.sys.init.AuthCache;
import dyl.sys.init.Cache;

public class ConfigValue extends TagSupport{
	private static final long serialVersionUID = 8125593281113591128L;
	private String type;
	public void setType(String type) {
		this.type = type;
	}
	private String key;
	public void setKey(String key) {
		this.key = key;
	}
	public int doStartTag() throws JspException {
		Writer out = pageContext.getOut();
		String str = "";
		try{
			if(StringUtils.isEmpty(type)){
				//str = AuthCache.configMap.get(key);
			}else{
				str =Cache.sysConfigMap.get(type).get(key);
				if(str==null)str="";
			}
			out.write(str.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
}
