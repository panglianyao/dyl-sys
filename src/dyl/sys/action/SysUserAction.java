package dyl.sys.action;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.LayPageData;
import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.Annotation.Auth;
import dyl.sys.Annotation.AuthAction;
import dyl.sys.bean.SysUser;
import dyl.sys.service.SysRoleServiceImpl;
import dyl.sys.service.SysRoleUserServiceImpl;
import dyl.sys.service.SysUserServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-03-22 21:57:10
 */
@Controller
public class SysUserAction extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private SysUserServiceImpl sysUserServiceImpl;
	@Resource
	private SysRoleServiceImpl sysRoleServiceImpl;
	@Resource
	private SysRoleUserServiceImpl sysRoleUserServiceImpl;
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 * @throws Exception 
	 */
	@Auth
	@RequestMapping(value = "/sysUser!main.do")
	public String  main(Page page,SysUser sysUser,HttpServletRequest request) throws Exception{
		return "/sys/sysUser/sysUserMain";
	}
	@Auth
	@ResponseBody
	@RequestMapping(value = "/sysUser!findSysUserList.do")
	public LayPageData  findSysUserList(Page page,SysUser sysUser,HttpServletRequest request){
		try {
			return getlayPageData(sysUserServiceImpl.findSysUserList(page, sysUser),page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/sysUser!sysUserForm.do")
	public String  sysUserForm(BigDecimal id,HttpServletRequest request){
		try {
			if(id!=null){
				request.setAttribute("sysUser",sysUserServiceImpl.getSysUser(id));
				//获取用户角色
				request.setAttribute("userRoles", sysRoleUserServiceImpl.querySysRoleUserList(id));
			}
			//查询可分配的角色
			request.setAttribute("authRoleList",sysRoleServiceImpl.findAuthSysRoleList());
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/sys/sysUser/sysUserForm";
	}
	/**
	 * 说明：更新或者插入SYS_USER
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.ADD)
	@ResponseBody()
	@RequestMapping(value = "/sysUser!add.do")
	public R add(SysUser sysUser,String[] roleIds,String[] oldRoleIds,HttpServletRequest request){
		try {
			sysUser.setPassword(sysUserServiceImpl.defaultPWD());
			int ret =  sysUserServiceImpl.insertSysUser(sysUser,roleIds,oldRoleIds,getSysUser(request));
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	@Auth(action=AuthAction.UPDATE)
	@ResponseBody()
	@RequestMapping(value = "/sysUser!update.do")
	public R update(SysUser sysUser,String[] roleIds,String[] oldRoleIds,HttpServletRequest request){
		try {
			int ret = sysUserServiceImpl.updateSysUser(sysUser,roleIds,oldRoleIds,getSysUser(request));
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表SYS_USER中的记录
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.DELETE)
	@ResponseBody()
	@RequestMapping(value = "/sysUser!delete.do")
	public R deleteSysUser(BigDecimal id,HttpServletRequest request){
		try {
			int ret = sysUserServiceImpl.deleteSysUser(id);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：重置密码
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.UPDATE)
	@ResponseBody()
	@RequestMapping(value = "/sysUser!rPwd.do")
	public R rPwd(BigDecimal id,HttpServletRequest request){
		try {
			int ret = sysUserServiceImpl.rPwd(id);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
}
