package dyl.sys.action;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.SpringContextUtil;
import dyl.sys.bean.MenuNav;
import dyl.sys.bean.SysMenu;
import dyl.sys.bean.Ztree;
import dyl.sys.service.SysAuthKindServiceImpl;
import dyl.sys.service.SysMenuServiceImpl;

@Controller
public class SysMenuAction extends BaseAction {
	protected Log log = LogFactory.getLog(getClass());
	@Resource
	private SysMenuServiceImpl sysMenuServiceImpl;
	@Resource
	private SysAuthKindServiceImpl sysAuthKindServiceImpl;
	
	@RequestMapping(value = "/sysMenu!main.do")
	public String main(HttpServletRequest request){
		return "/sys/sysMenu/sysMenuMain";
	}
	/**
	 * 获取系统左侧菜单
	 * @return
	 */
	@ResponseBody()
    @RequestMapping(value = "/sysMenu!getMenu.do")
	public List<MenuNav> getMenu(HttpServletRequest request){
    	return sysMenuServiceImpl.getMenu(getSysUser(request));
	}
	/**
	 * 获取可分配权限的菜单
	 * @return
	 */
	@ResponseBody()
    @RequestMapping(value = "/sysMenu!getMenuForZtree.do")
	public List<Ztree> getMenuForZtree(BigDecimal roleId){
    	return sysMenuServiceImpl.getMenuForZtree(roleId);
	}
	/**
	 * 菜单管理获取菜单
	 * @return
	 */
	@ResponseBody()
    @RequestMapping(value = "/sysMenu!getMenuForZtreeByManage.do")
	public List<Ztree> getMenuForZtreeByManage(){
    	return sysMenuServiceImpl.getMenuForZtreeByManage();
	}
	/**
	 * 新增菜单
	 * @return
	 */
	@ResponseBody()
    @RequestMapping(value = "/sysMenu!addMenu.do")
	public Ztree addMenu(Ztree ztree,HttpServletRequest request){
    	return sysMenuServiceImpl.addMenu(ztree,getSysUser(request));
	}
	/**
	 * 删除菜单
	 * @return
	 */
	@ResponseBody()
    @RequestMapping(value = "/sysMenu!delete.do")
	public R delete(BigDecimal id){
    	return returnByDbRet(sysMenuServiceImpl.delete(id));
	}
	/**
	 * 移动菜单
	 * @return
	 */
	@ResponseBody()
    @RequestMapping(value = "/sysMenu!move.do")
	public R move(String moveType,int moveId,int movePId,int targetId,int targetPId){
    	return returnByDbRet(sysMenuServiceImpl.move(moveType, moveId, movePId, targetId, targetPId));
	}
	/**
	 * 说明：添加或修改
	 * @return String
	 */
	@RequestMapping(value = "/sysMenu!sysMenuForm.do")
	public String  sysMenuForm(BigDecimal id,HttpServletRequest request){
		try {
			request.setAttribute("sysMenu",sysMenuServiceImpl.getSysMenu(id));
			request.setAttribute("kinds", sysAuthKindServiceImpl.findSysMenuKindList(id));
			//将系统中所有的action集合列出来
			RequestMappingHandlerMapping rmhp =  SpringContextUtil.getApplicationContext().getBean(RequestMappingHandlerMapping.class);  
	        Map<RequestMappingInfo, HandlerMethod> map = rmhp.getHandlerMethods();  
	        Set<String> actionLists = new HashSet<String>();
	        for(RequestMappingInfo info : map.keySet()){
	        	actionLists.add(map.get(info).getBean().toString());
	        } 
	        request.setAttribute("actionLists", actionLists);
		} catch (Exception e){
			throw new CommonException(request,e);
		}
		return "/sys/sysMenu/sysMenuForm";
	}
	/**
	 * 说明：更新或者插入SYS_ROLE
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/sysMenu!update.do")
	public R update(SysMenu sysMenu,String[] kind,HttpServletRequest request){
		try {
			int	ret = sysMenuServiceImpl.updateSysMenu(sysMenu,kind);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request,e);
		}
	}
}
