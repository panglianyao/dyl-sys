package dyl.sys.action;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.Annotation.Auth;
import dyl.sys.Annotation.AuthAction;
import dyl.sys.bean.Test;
import dyl.sys.service.TestServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-06-16 16:51:55
 */
@Controller
public class TestAction extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private TestServiceImpl testServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/test!main.do")
	public String main(Page page,Test test,HttpServletRequest request){
		try{
			request.setAttribute("testList",testServiceImpl.findTestList(page, test));
			request.setAttribute("test", test);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/sys/test/testMain";
	}
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/test!testForm.do")
	public String  testForm(Integer id,HttpServletRequest request){
		try {
			if(id!=null)request.setAttribute("test",testServiceImpl.getTest(id));
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/sys/test/testForm";
	}
	/**
	 * 说明：插入t_test
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.ADD)
	@ResponseBody()
	@RequestMapping(value = "/test!add.do")
	public R add(Test test,HttpServletRequest request){
		try {
			//test.setCreator(getSysUser(request).getId());
			int ret = testServiceImpl.insertTest(test);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新t_test
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.UPDATE)
	@ResponseBody()
	@RequestMapping(value = "/test!update.do")
	public R update(Test test,HttpServletRequest request){
		try {
			int ret = testServiceImpl.updateTest(test);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表t_test中的记录
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.DELETE)
	@ResponseBody()
	@RequestMapping(value = "/test!delete.do")
	public R deleteTest(String dataIds,HttpServletRequest request){
		try {
			int[] ret = testServiceImpl.deleteTest(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
}
