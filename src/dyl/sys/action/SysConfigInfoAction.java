package dyl.sys.action;


import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.LayPageData;
import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.Annotation.Auth;
import dyl.sys.Annotation.AuthAction;
import dyl.sys.bean.SysConfigInfo;
import dyl.sys.service.SysConfigInfoServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-11-09 20:06:54
 */
@Controller
public class SysConfigInfoAction extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private SysConfigInfoServiceImpl sysConfigInfoServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 * @throws Exception 
	 */
	@Auth
	@RequestMapping(value = "/sysConfigInfo!main.do")
	public String main(SysConfigInfo sysConfigInfo,HttpServletRequest request) throws Exception{
		request.setAttribute("configInfoList", sysConfigInfoServiceImpl.findSysConfigInfoList(sysConfigInfo));
		request.setAttribute("sysConfigInfo", sysConfigInfo);
		return "/sys/sysConfigInfo/sysConfigInfoMain";
	}
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/sysConfigInfo!sysConfigInfoForm.do")
	public String  sysConfigInfoForm(BigDecimal id,String type,HttpServletRequest request){
		try {
			if(id!=null)request.setAttribute("sysConfigInfo",sysConfigInfoServiceImpl.getSysConfigInfo(id));
			request.setAttribute("type", type);
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/sys/sysConfigInfo/sysConfigInfoForm";
	}
	/**
	 * 说明：插入SYS_CONFIG_INFO
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.ADD)
	@ResponseBody()
	@RequestMapping(value = "/sysConfigInfo!add.do")
	public R add(SysConfigInfo sysConfigInfo,HttpServletRequest request){
		try {
			//sysConfigInfo.setCreator(getSysUser(request).getId());
			int ret = sysConfigInfoServiceImpl.insertSysConfigInfo(sysConfigInfo);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新SYS_CONFIG_INFO
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.UPDATE)
	@ResponseBody()
	@RequestMapping(value = "/sysConfigInfo!update.do")
	public R update(SysConfigInfo sysConfigInfo,HttpServletRequest request){
		try {
			int ret = sysConfigInfoServiceImpl.updateSysConfigInfo(sysConfigInfo);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表SYS_CONFIG_INFO中的记录
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.DELETE)
	@ResponseBody()
	@RequestMapping(value = "/sysConfigInfo!delete.do")
	public R deleteSysConfigInfo(String dataIds,HttpServletRequest request){
		try {
			int[] ret = sysConfigInfoServiceImpl.deleteSysConfigInfo(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
}
