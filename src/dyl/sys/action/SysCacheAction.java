package dyl.sys.action;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysFileinfo;
import dyl.sys.init.AuthCache;
import dyl.sys.init.Cache;
import dyl.sys.service.SysFileinfoServiceImpl;
/**
 * 缓存总管理
 * @author Dyl
 * 2017-06-06 15:31:42
 */
@Controller
public class SysCacheAction extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private SysFileinfoServiceImpl sysFileinfoServiceImpl;
	@Resource
	private Cache wCache;
	@Resource
	private AuthCache authCache;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@RequestMapping(value = "/cache!main.do")
	public String main(String cacheName,HttpServletRequest request){
		try{
			request.setAttribute("selectDataMap", Cache.selectDataMap);
			request.setAttribute("sysConfigMap", Cache.sysConfigMap);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/sys/sysCache/sysCacheMain";
	}
	@RequestMapping(value = "/cache!form.do")
	public String form(String type,String cacheMap,HttpServletRequest request){
		try{
			if(cacheMap.equals("sysConfig")){
				request.setAttribute("map",Cache.sysConfigMap.get(type));
			}
			request.setAttribute("type",type);
			request.setAttribute("cacheMap",cacheMap);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/sys/sysCache/sysCacheForm";
	}
	@ResponseBody
	@RequestMapping(value = "/czhc.do")
	public R czhc(String type,String cacheMap,HttpServletRequest request){
		try{
			if(cacheMap.equals("sysConfig")){
				wCache.loadSysConfigByType(type);
			}
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return new R(true);
	}
	@ResponseBody
	@RequestMapping(value = "/qbcz.do")
	public R qbcz(HttpServletRequest request){
		try{
			wCache.load();
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return new R(true);
	}
	/**
	 * 重载权限
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/authCz.do")
	public R authCz(HttpServletRequest request){
		try{
			authCache.load();
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return new R(true);
	}
}
