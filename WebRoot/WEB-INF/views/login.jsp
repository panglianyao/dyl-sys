<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="zh">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>登录</title>
<%@include file="common/commonCss.jsp"%>
<link rel="stylesheet" type="text/css" href="${res}/css/login.css">
</head>
<body>
<div class="htmleaf-container">
	<div class="wrapper">
		<div class="container">
			<h1>欢迎</h1>
			
			<form class="form" id="loginForm" action="login.do"  method="post">
				<input type="text"  name="username" placeholder="用户名">
				<input type="password" name="password" placeholder="密码">
				<button type="submit" id="login-button">登陆</button>
			</form>
		</div>
		
		<ul class="bg-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
</div>

<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
<script>
$('#login-button').click(function (event) {
	event.preventDefault();
	$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
	setTimeout(function(){
		$('#loginForm').submit();
	},600); 
	
});
<c:if test="${not empty loginMsg}"> layer.alert("${loginMsg}",{icon:5});</c:if>
</script>

<div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';color:#000000">
<h1>后台管理</h1>
</div>
</body>
</html>