<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>通用上传</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>
	<body>
		<div class="admin-main">
		<!-- 查询条件 -->
			<form  class="layui-form dyl-admin"  id="mainForm" method="post">
				<div class="layui-inline fr">
					<a href="javascript:;" class="layui-btn layui-btn-sm" data-opt="czhc">
						重载缓存
					</a>
				</div>
			</form>
			<div class="layui-form news_list" id="bjTable">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
								<th>type</th>
								<th>key</th>
								<th>value</th>
								<!-- <th>说明</th> -->
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${map}" var="m">
								<tr>
							        <td><input type="checkbox" lay-skin="primary" data-opt="check" data-id="${m.key}"></td>
									<td>${type}</td>
									<td>${m.key}</td>
									<td>${m.value}</td>
									<%-- <td>${o.fileSize}</td> --%>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			$(document).on("click","[data-opt='czhc']",function(){
				getJsonDataByPost("czhc.do","type=${type}&cacheMap=${cacheMap}",function(data){
					if(data.result){
						layer.msg("重载成功!");
					}else{
						l.msg(data.msg);//错误消息弹出
					}
				},true); 
			});
		</script>
	</body>
</html>