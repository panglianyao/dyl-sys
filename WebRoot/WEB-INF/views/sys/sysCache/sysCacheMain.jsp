<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>通用上传</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>
	<body>
		<div class="admin-main">
			<form  class="layui-form dyl-admin"  id="mainForm" method="post">
				<div class="layui-inline fr">
					<a href="javascript:;" class="layui-btn layui-btn-sm" data-opt="qbcz">
						全部重载
					</a>
				</div>
				<div class="layui-inline fr">
					<a href="javascript:;" class="layui-btn layui-btn-sm" data-opt="authCz">
						重载权限
					</a>
				</div>
			</form>
			<fieldset class="layui-elem-quote layui-elem-field" >
				<legend>系统配置表缓存</legend>
				<div  class="layui-form-item"  id="mainForm" method="post">
					<c:forEach items="${sysConfigMap}" var="c">
						<div class="layui-inline">
							<button class="layui-btn layui-btn-sm" data-type="sysConfig" data-id="${c.key}">${c.key}</button>
				        </div>
			        </c:forEach>
			    </div>
		    </fieldset>
		    <fieldset class="layui-elem-quote layui-elem-field" >
				<legend>查询语句缓存</legend>
			    <div  class="layui-form-item"  id="mainForm" method="post">
					<c:forEach items="${selectDataMap}" var="c">
						<div class="layui-inline">
							<button class="layui-btn layui-btn-sm" data-type="selectData" data-id="${c.key}">${c.key}</button>
				        </div>
			        </c:forEach>
			    </div>
		    </fieldset>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			$('[data-type="sysConfig"]').click(function(){
				var para={
						url:"cache!form.do",
					 	para:"type="+$(this).data("id")+"&cacheMap="+$(this).data("type")
					};
					addOrUpdate(para);//通用新增修改方法
			});
			$(document).on("click","[data-opt='qbcz']",function(){
				getJsonDataByPost("qbcz.do",'',function(data){
					if(data.result){
						layer.msg("重载成功!");
					}else{
						l.msg(data.msg);//错误消息弹出
					}
				},true); 
			});
			$(document).on("click","[data-opt='authCz']",function(){
				getJsonDataByPost("authCz.do","",function(data){
					if(data.result){
						layer.msg("重载成功!");
					}else{
						l.msg(data.msg);//错误消息弹出
					}
				},true); 
			});
		</script>
	</body>
</html>