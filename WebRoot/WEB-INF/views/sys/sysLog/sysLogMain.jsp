<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>系统日志管理</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>
	
	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote news_search" action="sysLog!main.do" id="mainForm" method="post">
    			<div class="layui-inline">
			        <label class="layui-form-label">url地址:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="url"  value="${sysLog.url}" placeholder="请输入" class="layui-input">
			        </div>
			    </div>
				<div class="layui-inline">
					<a href="javascript:;" class="layui-btn layui-btn-sm" id="search" onclick="reloadTable('sysLogTable');">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<a href="javascript:;" class="layui-btn layui-btn-sm" id="rzDownload">
						<i class="layui-icon">&#xe615;</i> 下载系统日志
					</a>
				</div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-form news_list">
					<table  lay-data="{${layUiTableConfig},url:'sysLog!findSysLogList.do',id:'sysLogTable'}" data-anim="layui-anim-up" class="layui-table" >
					  <thead>
					    <tr>
					   	  <!-- <th lay-data="{checkbox:true, fixed: true}"></th> -->
						  <th lay-data="{field:'url', width:160}">url地址</th>
						  <th lay-data="{field:'para', width:160}">参数</th>
						  <th lay-data="{field:'loginuser', width:160}">登陆用户</th>
						  <th lay-data="{field:'ip', width:160}">Ip地址</th>
						  <th lay-data="{field:'time', width:180}">访问时间</th>
					    </tr>
					  </thead>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			//日志下载
			$('#rzDownload').click(function(){
				layer.open({
					type: 2,
					title: "下载系统日志",
					content: "log!main.do",
					maxmin: true,
					shade: true,
					area: [$(window).width()<500?$(window).width():($(window).width()/2+'px'), ($(window).height()-100)+"px"],
					scrollbar:false,
					shade:0.5,//遮罩透明度
				});
			});
		</script>
	</body>
</html>