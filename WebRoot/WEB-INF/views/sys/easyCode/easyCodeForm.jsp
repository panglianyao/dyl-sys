<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form form-content" action="easyCode.do"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="name" value="${easyCode.name}" />
	<div class="layui-form-item">
		<label class="layui-form-label">表/视图名</label>
		<div class="layui-input-inline"><label class="layui-form-label">${easyCode.name}</label></div>
    </div>
    <div class="layui-form-item">
	    <label class="layui-form-label">主键</label>
	    <div class="layui-input-inline">
	      <input type="text" name="pKey" required  lay-verify="required" placeholder="请输入名称" value="id" class="layui-input"/>
	    </div>
  	</div>
    <div class="layui-form-item">
      <label class="layui-form-label">模板</label>
      <div class="layui-input-block">
       	  <input type="checkbox" name="templates" checked="checked" value="javaBean-layui.ftl@${table.className}.java" title="javaBean">
       	  <input type="checkbox" name="templates" checked="checked" value="javaAction-layui.ftl@${table.className}Action.java" title="Action">
       	  <input type="checkbox" name="templates" checked="checked" value="javaServiceImpl-layui.ftl@${table.className}ServiceImpl.java" title="service">
       	  <input type="checkbox" name="templates" checked="checked" value="viewMain-layui.ftl@${table.javaProperty}Main.jsp" title="mainJsp">
       	  <input type="checkbox" name="templates" checked="checked" value="viewForm-layui.ftl@${table.javaProperty}Form.jsp" title="formJsp">
   	  </div>
    </div>
    <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 100px;">是否需要增/改</label>
	    <div class="layui-input-block">
	       <input type="checkbox" checked="" name="hasEdit" value="true" lay-skin="switch" lay-filter="switchTest" lay-text="是|否">
	    </div>
  	</div>
  	<div class="layui-form-item">
	    <label class="layui-form-label" style="width: 100px;">是否需要删除</label>
	    <div class="layui-input-block">
	       <input type="checkbox" checked="" name="hasDelete" value="true" lay-skin="switch" lay-filter="switchTest" lay-text="是|否">
	    </div>
  	</div>
  	<div class="layui-form-item">
	    <label class="layui-form-label" style="width: 100px;">是否需要导出</label>
	    <div class="layui-input-block">
	       <input type="checkbox" checked="" name="hasExport" value="true" lay-skin="switch" lay-filter="switchTest" lay-text="是|否">
	    </div>
  	</div>
  	<div class="layui-form-item">
	    <label class="layui-form-label" style="width: 100px;">是否需要权限</label>
	    <div class="layui-input-block">
	       <input type="checkbox" checked="" name="hasAuth" value="true" lay-skin="switch" lay-filter="switchTest" lay-text="是|否">
	    </div>
  	</div>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="saveOrUpdate" lay-submit class="layui-hide" />
</form>
<script>
	form.render();//重新渲染表单
	form.on('submit(saveOrUpdate)', function(data){//表单提交方法
		location.href="easyCode.do?"+$('#saveOrUpdateForm').serialize();
 		/* getJsonByajaxForm("saveOrUpdateForm","",function(data){
			if(data.result){
				layer.closeAll('page'); //关闭信息框
				layer.alert($('.layui-layer-btn0').text()+"成功!",function(){
					$('#search').click();//查询
				}); 
			}else{
				l.msg(data.msg);//错误消息弹出
			}
		},true);  */
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	}); 
</script>
