<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<title>菜单管理</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
		<%@include file="/WEB-INF/views/common/ztreeCss.jsp"%>
	</head>
	<body>
		<div class="layui-row">
		    <div class="layui-col-xs12 layui-col-md4">
		      <fieldset class="layui-elem-field layui-field-title">
				  <legend>菜单</legend>
				</fieldset>
		 		<ul id="authTree" class="ztree" style="margin-top:0; width:160px;"></ul>
		    </div>
		    <div class="layui-col-xs12 layui-col-md8" id="mainRight">
		      
		    </div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script type="text/javascript" src="${res}/zTree/js/jquery.ztree.all.min.js"></script>
		<script>
			var setting = {
					view: {
						addHoverDom: function addHoverDom(treeId, treeNode){//显示新增按钮
							if(treeNode.level>1||$("#addBtn_"+treeNode.tId).length>0)return;//菜单只允许二级节点
							var addStr = "<span class='button add' id='addBtn_" + treeNode.tId+ "' title='新增菜单' onfocus='this.blur();'></span>";
							$("#" + treeNode.tId + "_span").after(addStr);
							var btn = $("#addBtn_"+treeNode.tId);
							if (btn) btn.bind("click", function(){
								var newNode = {pId:treeNode.id};
								getJsonDataByPost("sysMenu!addMenu.do","pId="+newNode.pId,function(data){
									newNode['id']=data.id;//绑定节点id
									newNode['name']=data.name;//绑定节点id
									newNode = zTree.addNodes(treeNode,newNode);//新增节点
									zTree.selectNode(newNode[0]);//选择节点
									editMenu(newNode[0]);
									return false;
								},true);
							}); 
						},
						removeHoverDom:function(treeId, treeNode){//删除新增按钮，取消绑定事件
							$("#addBtn_"+treeNode.tId).unbind().remove();
						},
						selectedMulti: false//多选关闭
					},
					edit: {
						enable: true,
						showRenameBtn: false,
						showRemoveBtn: function(treeId, treeNode){//显示删除按钮
							return treeNode.id>10;
						}
					}, 
					data: {
						simpleData: {
							enable: true
						}
					},
					callback: {
						onClick: function(event, treeId, treeNode){
							editMenu(treeNode);
						},
						beforeDrag: function(treeId, treeNodes){//控制节点不允许拖动
							if(treeNodes[0].id==0)return false;
							return true;
						},
						beforeDrop: function(treeId, treeNodes, targetNode, moveType){//移动节点
							if(moveType == "inner" && treeNodes[0].pId==0){
								layer.alert("根节点不允许变更为子节点!");
								return false;
							}
							if(moveType == "inner" && targetNode.pId!=0){
								layer.alert("最多只能变更为二级节点!");
								return false;
							}
							getJsonDataByPost("sysMenu!move.do","moveType="+moveType+"&moveId="+treeNodes[0].id+"&movePId="+treeNodes[0].pId+"&targetId="+targetNode.id+"&targetPId="+targetNode.pId,function(data){
								if(data.result){
									layer.msg("移动成功!");
								}else{
									l.msg(data.msg);
								}
								layer.close(index);
							},true); 
							return true;
						},
						beforeRemove:function(treeId, treeNode){ //删除操作
							layer.confirm('即将删除该该菜单及菜单的子节点，是否继续?', function(index){
								getJsonDataByPost("sysMenu!delete.do","id="+treeNode.id,function(data){
									if(data.result){
										layer.msg("删除成功!");
										zTree.removeNode(treeNode);
									}else{
										l.msg(data.msg);
									}
									layer.close(index);
								},true); 
							});
							return false;
						}
					}
				};
				$('#authRole').click(function(){
					$("#treeDiv").toggle("fast");
				});
				var  zTree;
				$(function(){
					getJsonDataByPost("sysMenu!getMenuForZtreeByManage.do","",function(data){
						zTree = $.fn.zTree.init($("#authTree"),setting,data);
					});
				});
				function editMenu(treeNode){ 
					getHtmlDataByPost("sysMenu!sysMenuForm.do","id="+treeNode.id,function(data){
						$('#mainRight').html(data);
					},true);
				}
		</script>
	</body>
</html>