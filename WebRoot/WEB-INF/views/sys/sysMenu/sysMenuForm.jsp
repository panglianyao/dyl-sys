<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<fieldset class="layui-elem-field layui-field-title">
	<legend>${sysMenu.title}</legend>
</fieldset>
<form class="layui-form form-content"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="id" value="${sysMenu.id}" />
	<div class="layui-form-item">
		<label class="layui-form-label">标题</label>
		<div class="layui-input-inline">
		  <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" value="${sysMenu.title}" class="layui-input"  />
		</div>
    </div>
	<div class="layui-form-item">
		<label class="layui-form-label">链接</label>
		<div class="layui-input-inline">
		  <input type="text" name="url"  placeholder="请输入链接" value="${sysMenu.url}" class="layui-input"  />
		</div>
    </div>
  <div class="layui-form-item">
    <label class="layui-form-label">状态</label>
    <div class="layui-input-block">
      <input type="radio" name="state" value="0" title="正常" ${sysMenu.state eq '0'?'checked':''}  ${empty sysMenu?'checked':''} />
      <input type="radio" name="state" value="1" title="禁用" ${sysMenu.state eq '1'?'checked':''} />
    </div>
  </div>
  <div class="layui-form-item">
	<label class="layui-form-label">图标</label>
	<div class="layui-input-inline">
	  <input type="text" name="icon"  placeholder="请输入图标" value="${sysMenu.icon}" class="layui-input"  />
	</div>
	<div class="layui-form-mid layui-word-aux"><a href="http://www.layui.com/doc/element/icon.html#use" class="layui-btn layui-btn-xs" target="_blank">内置图标库</a></div>
	<!-- <div class="layui-form-mid layui-word-aux"><a href="http://www.fontawesome.com.cn/faicons/" class="layui-btn layui-btn-xs" target="_blank">fa图标库</a></div> -->
   </div>
   <div class="layui-inline">
      <label class="layui-form-label">actionClass</label>
      <div class="layui-input-inline">
        <select name="actionClass"  lay-search="">
          <option value="">请选择</option>
          <c:forEach items="${actionLists}" var="a">
          		<option value="${a}"  ${sysMenu.actionClass eq a ?'selected="selected"':''}  >${a}</option>
          </c:forEach>
        </select>
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">权限种类
      <a href="javascript:;" class="layui-btn layui-btn-xs" id="authKind">
			<i class="layui-icon">&#xe642;</i> 维护
	  </a></label>
      <div class="layui-input-block">
       <c:forEach items="${kinds}" var="k">
       	  <input type="checkbox" name="kind"
	       	  	 ${not empty k.ck?'checked="checked"':''}
       	  value="${k.id}" title="${k.name}">
       </c:forEach>
   	  </div>
    </div>
    <div class="layui-form-item">
		<label class="layui-form-label">备注</label>
		<div class="layui-input-inline">
		  <textarea placeholder="请输入备注" class="layui-textarea" name="note">${sysMenu.note}</textarea>
		</div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	       <button class="layui-btn" lay-submit lay-filter="update">保存</button>
	    </div>
	</div>
</form>
<script>
	form.render();//重新渲染表单
	form.on('submit(update)', function(data){//表单提交方法
 		getJsonByajaxForm("saveOrUpdateForm","sysMenu!update.do",function(data){
			if(data.result){
				l.msg("修改成功!");
			}else{
				l.msg(data.msg);//错误消息弹出
			}
		},true); 
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	});
	//权限种类维护
	$('#authKind').click(function(){
		layer.open({
			type: 2,
			title: "权限种类",
			content: 'sysAuthKind!main.do',
			shade: true,
			area: ['500px', '400px'],
			maxmin: true,
			scrollbar:false,
			shade:0.5,//遮罩透明度
			success:function(layero, index){
				
			}
		});
		return false;
	});
</script>
