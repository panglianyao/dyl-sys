<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<title>定时任务配置</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
			<!-- 查询条件 -->
			<form  class="layui-elem-quote news_search" action="sysQuartz!main.do" id="mainForm" method="post">
    			<div class="layui-inline">
			        <label class="layui-form-label">触发器名称:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="triggername"  value="${sysQuartz.triggername}" placeholder="请输入触发器名称" class="layui-input">
			        </div>
			    </div>
				<div class="layui-inline">
					<a href="javascript:;" class="layui-btn layui-btn-sm" id="search" onclick="reloadTable('sysQuartzTable');">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<a href="javascript:;" class="layui-btn layui-btn-sm" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
					<a href="javascript:;" class="layui-btn layui-btn-sm" id="delete">
						<i class="layui-icon">&#xe640;</i> 删除
					</a>
				</div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<!-- 主table -->
				<div class="layui-form news_list">
					<table  lay-data="{${layUiTableConfig},url:'sysUser!findSysQuartzList.do',id:'sysQuartzTable'}" data-anim="layui-anim-up" class="layui-table" >
					  <thead>
					    <tr>
					   	  <th lay-data="{checkbox:true, fixed: true}"></th>
						  <th lay-data="{field:'triggername', width:160}">触发器名称</th>
						  <th lay-data="{field:'cronexpression', width:160}">时间表达式</th>
						  <th lay-data="{field:'jobdetailname', width:120}">脚本名称</th>
						  <th lay-data="{field:'targetobject', width:130}">类名称(首字母小写)</th>
						  <th lay-data="{field:'methodname', width:130}">方法名</th>
						  <th lay-data="{templet:'#templte1', width:80}">是否并发启动任务</th>
						  <th lay-data="{templet:'#templte2',width:80}">状态</th>
						  <th lay-data="{fixed: 'right', width:140, align:'center', toolbar: '#barDemo'}">操作</th>
					    </tr>
					  </thead>
					</table>
				</div>
			</fieldset>
		 <script type="text/html" id="templte1">
            {{#  if(d.concurrent==1){ }}
             	<i class="layui-btn layui-btn-normal layui-btn-xs">并发</i>
            {{#  }else{ }}
             	<i class="layui-btn layui-btn-danger layui-btn-xs">非并发</i>
            {{#  }  }}
	     </script>
	     <script type="text/html" id="templte2">
            {{#  if(d.state==1){ }}
             	<i class="layui-btn layui-btn-normal layui-btn-xs">启用</i>
            {{#  }else{ }}
             	<i class="layui-btn layui-btn-danger layui-btn-xs">关闭</i>
            {{#  }  }}
	     </script>
		<script type="text/html" id="barDemo">
			<a href="javascript:;" class="layui-btn layui-btn-xs" data-id="{{d.id}}" data-opt="edit">
				<i class="layui-icon">&#xe642;</i> 修改
			</a>
	     </script>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			//添加方法
			$('#add').click(function(){
				var para={
					url:"sysQuartz!sysQuartzForm.do",
					title:"添加",
					btnOK:"保存"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//修改方法
			$('[data-opt=edit]').die().live("click",function(){
				var para={
					url:"sysQuartz!sysQuartzForm.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改",
					btnOK:"修改"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//删除方法
			$('#delete').die().live("click",function(){
				var dataIds = getTableIds("sysQuartzTable");
				if(dataIds){
					layer.confirm('确认删除所选择的吗?', function(index){
						getJsonDataByPost("sysQuartz!delete.do","dataIds="+dataIds,function(data){
							if(data.result){
								layer.msg("删除成功!"); 
								$('#search').click();//查询
							}else{
								l.msg(data.msg);//错误消息弹出
							}
							layer.close(index);//关闭删除确认提示框
						},true); 
					});
				}
			});
		
		</script>
	</body>
</html>