<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form form-content" action="sysRole!saveOrUpdate.do"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="id" value="${roleId}" />
	<div id="treeDiv" >
		<ul id="authTree" class="ztree" ></ul>
	</div>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="updateAuth" lay-submit class="layui-hide" />
</form>
<script type="text/javascript">
		var setting = {
			check: {
				enable: true,
				autoCheckTrigger:true//自动关联勾选
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};
		$('#authRole').click(function(){
			$("#treeDiv").toggle("fast");
		});
		$(function(){
			getJsonDataByPost("sysMenu!getMenuForZtree.do","roleId=${roleId}",function(data){
				$.fn.zTree.init($("#authTree"),setting,data);
			});
		});
		$('[lay-filter=updateAuth]').click(function(){
			var nodes=$.fn.zTree.getZTreeObj("authTree").getCheckedNodes(true);
			var authRoleKinds  = '';
			$(nodes).each(function(i,v){
				if(v.id.indexOf("_")!=-1)authRoleKinds+=v.id+",";
			});
			authRoleKinds = removeLastChar(authRoleKinds); 
			getJsonDataByPost("sysRole!saveAuth.do","authRoleKinds="+authRoleKinds+"&roleId=${roleId}",function(data){
				if(data.result){
					layer.closeAll('page'); //关闭信息框
					l.msg("权限修改成功!");
				}else{
					l.msg(data.msg);//错误消息弹出
				}
			},true);
			return false;
		});
		form.on('submit(saveOrUpdate)', function(data){//表单提交方法
			/* //获取选中的菜单Id
	 		getJsonByajaxForm("saveOrUpdateForm","sysRole!saveOrUpdate.do",function(data){
				if(data.result){
					layer.closeAll('page'); //关闭信息框
					layer.alert($('.layui-layer-btn0').text()+"成功!",function(){
						$('#search').click();//查询
					}); 
				}else{
					l.msg(data.msg);//错误消息弹出
				}
			},true); */
			return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
		});
</script>