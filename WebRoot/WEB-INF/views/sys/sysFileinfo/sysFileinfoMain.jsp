<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>通用上传</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>
	<style>
			.odUploadList {
			    float: left;
			    width: 700px;
			    border-top: #ccc 1px dashed;
			    margin-left: 50px;
			    margin-bottom: 30px;
			}
			.odUploadList li {
			    float: left;
			    width: 100%;
			    height: 48px;
			    line-height: 48px;
			    border-bottom: #ccc 1px dashed;
			}
			li {
			    list-style: none;
			}
			.odUploadList li em.odUploadListName {
			    width: 240px;
			    padding-left: 30px;
			    margin-left: 15px;
			    background: url(${res}/images/odUploadListName.png) no-repeat left;
			}
			.odUploadList li em {
			    position: relative;
			    float: left;
			    font-size: 12px;
			    text-overflow: ellipsis;
			    white-space: nowrap;
			    overflow: hidden;
			}
			.odUploadListBar {
			    width: 180px;
			    height: 6px;
			    background: #dedede;
			    border-radius: 3px;
			    margin-top: 21px;
			    margin-right: 10px;
			}
			.odUploadList li em {
			    position: relative;
			    float: left;
			    font-size: 12px;
			    color: #4a4a4a;
			    text-overflow: ellipsis;
			    white-space: nowrap;
			    overflow: hidden;
			}
			.odUploadList li em.odUploadClose {
			    float: right;
			    width: 10px;
			    height: 10px;
			    background: url(${res}/images/odClose.png) no-repeat center;
			    background-position: 0 0;
			    cursor: pointer;
			    margin-right: 15px;
			    margin-top: 22px;
			</style>
	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="sysFileinfo!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-sm" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<div class="layui-box layui-upload-button">
						<span class="layui-upload-icon" id="uploadFile"><i class="layui-icon"></i></span>
					</div>
				</div>
    			<div class="layui-input-inline" >
			        <label class="layui-form-label">上传文件名称:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="uploadName"  value="${sysFileinfo.uploadName}" placeholder="请输入上传文件名称" class="layui-input">
			        </div>
			    </div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
								<th>文件存储的真实名称</th>
								<th>上传文件名称</th>
								<th>文件大小</th>
								<th>文件下载地址</th>
								<th>上传时间</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${sysFileinfoList}" var="o">
								<tr>
							        <td><input type="checkbox" lay-skin="primary" data-opt="check" data-id="${o.id}"></td>
									<td><a href="${o.fileUrl}" target="_blank">${o.realName}</a></td>
									<td>${o.uploadName}</td>
									<td>${o.fileSize}</td>
									<td><a href="${o.fileUrl}">${o.fileUrl}</a></td>
									<td><fmt:formatDate value="${o.createTime}" pattern="yyyy-MM-dd HH:mm" /></td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<div id="uploadDiv" style="display:none">
			<ul class="odUploadList">
				
			</ul>
		</div>
		<script type="text/template" id="uploadQue">
			<em class="odUploadListName" lg="fileName"></em> 
				<em class="odUploadListBar">
					<div class="layui-progress">
					  <div class="layui-progress-bar layui-bg-blue" lg="progress" lay-percent="0%" style="width:0%"></div>
					</div>
				</em>
				<em class="odUploadListWords" lg="progress2"></em>
			    <em class="odUploadClose" style="" lg="close"></em>
		</script>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<!-- 基于Html5上传的插件 -->
		<script type="text/javascript" src="${res}/js/jquery.Huploadify.js"></script>
		<!-- 分页 -->
		<%@include file="/WEB-INF/views/common/page.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			var uploadDivId = 0;
			//上传文件初始化
			$('#uploadFile').Huploadify({
		        'uploader' : '${ctx}/upload.do',    //指定服务器端上传处理文件
		        'buttonText':"上传文件",
		        'fileTypeExts':'*',
		        'multi':true,//能否选择多个稿件
		        'successTimeout':300,//上传多久后会超时
		        'auto':true,//是否选择完文件后立即上传
		        "fileSizeLimit":1024*10,//上传文件大小限制--10M (Kb)
		        'onUploadSuccess' : function(file,data,response){
		        	var data = eval("(" + data + ")");
		        	if(data.result){
		        		alert(data.realName+","+data.size+","+file.name);
		        	}
		        	$('[lg="close"]').click(function(){//删除方法
						$(this).parent().hide();
					});
		        },
		        'onProgress':function(file,precent){//进度条方法
		        	$('#file'+file.index+' [lg="progress"]').css("width",precent);
		   			$('#file'+file.index+' [lg="progress2"]').text(precent);
		        },
		        'onSelect':function(file){//点开上传文件后的方法
		        	//加载上传进度
		        	$('.odUploadList').append("<li id=file"+file.index+">"+$('#uploadQue').html()+"</li>");
		        	$('#file'+file.index+" [lg='fileName']").text(file.name);
		        	if($('#layui-layer'+uploadDivId).length==0){
			        	//弹出上传进度框
			        	uploadDivId = layer.open({
							type: 1,
							title: "文件上传",
							content: $('#uploadDiv').html(),
							shade: true,
							area: ['800px', '400px'],
							maxmin: true,
							scrollbar:false,
							shade:0.5,//遮罩透明度
						});
		        	}
		        }
		    });
		
			form.render();//重新渲染表单
		</script>
	</body>
</html>