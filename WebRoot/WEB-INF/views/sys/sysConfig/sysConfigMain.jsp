<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>值列表维护</title>
<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
</head>

<body>
	<div class="layui-row">
		<div class="layui-col-md5">
			<div class="admin-main">
				<!-- 查询条件 -->
				<form class="layui-elem-quote  layui-form layui-form-item" id="mainForm1"
					method="post">
					<input type="hidden" name="type" value="${type}">
					<div class="layui-inline">
						<div class="layui-input-inline">
							<input type="text" name="cKey" placeholder="请输入键"
								class="layui-input">
						</div>
					</div>
					<div class="layui-inline fr">
						<a href="javascript:;" class="layui-btn layui-btn-sm"
							id="search" onclick="searchForm1();"> <i class="layui-icon">&#xe615;</i>
							查询
						</a>
						<dyl:hasPermission kind="ADD" menuId="${menuId}">
							<a href="javascript:;" class="layui-btn layui-btn-sm"
								id="add1"> <i class="layui-icon">&#xe608;</i> 添加
							</a>
						</dyl:hasPermission>
					</div>
				</form>
				<!-- 主table -->
				<div class="layui-form news_list" id="sysConfigTable">
					<table lay-data="{method:'post',limits:[12,30],page:true,limit:12,id:'sysConfigTable'}"
						data-anim="layui-anim-up" class="layui-table">
						<thead>
							<tr>
								<!-- <th lay-data="{checkbox:true, fixed: true}"></th> -->
								<th lay-data="{field:'cKey', width:138}">键</th>
								<th lay-data="{field:'cValue', width:138}">值</th>
								<th
									lay-data="{fixed: 'right', width:160, align:'center', toolbar: '#toolbar1'}">操作</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<div class="layui-col-md7" id="configInfo"></div>

	</div>
	<script type="text/html" id="toolbar1">
				<dyl:hasPermission kind="UPDATE" menuId="${menuId}">
				<a href="javascript:;" class="layui-btn layui-btn-xs" data-id="{{d.id}}" data-opt="edit1">
					<i class="layui-icon">&#xe642;</i> 修改
				</a>
				</dyl:hasPermission>
				<a href="javascript:;" class="layui-btn layui-btn-xs" data-id="{{d.cKey}}" data-opt="showChild">
					 子配置表
				</a>
	     </script>
	<!-- 通用js -->
	<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
	<script>
		//table自适应
		function searchForm1() {
			showLoading();
			table.reload('sysConfigTable', {
				url : 'sysConfig!findSysConfigList.do',
				where : getFormArr("mainForm1"),//获取form的参数
				done : function(res, curr, count) {
					hideLoading();
				}
			});
		}
		searchForm1();
		//添加方法
		$('#add1').click(function() {
			var para = {
				url : "sysConfig!sysConfigForm.do",
				title : "添加",
				btnOK : "保存"
			};
			addOrUpdate(para);//通用新增修改方法
		});
		//修改方法
		$('[data-opt=edit1]').die().live("click", function() {
			var para = {
				url : "sysConfig!sysConfigForm.do",
				para : "id=" + $(this).attr("data-id"),
				title : "修改",
				btnOK : "修改"
			};
			addOrUpdate(para);//通用新增修改方法
		});
		var fatherKey = "";
		$('[data-opt="showChild"]').die().live("click",function(){
			fatherKey = $(this).attr("data-id");
			searchForm2();
		});
		function searchForm2() {
			getHtmlDataByPost("sysConfigInfo!main.do?type="+fatherKey,$('#mainForm2').serialize(), function(html) {
				$('#configInfo').html(html);
			},true);
		}
		$('#add2').die().live("click",function() {
			var para = {
				url : "sysConfigInfo!sysConfigInfoForm.do?type="+fatherKey,
				title : "添加",
				btnOK : "保存"
			};
			addOrUpdate(para);//通用新增修改方法
		});
		//修改方法
		$('[data-opt=edit2]').die().live("click", function() {
			var para = {
				url : "sysConfigInfo!sysConfigInfoForm.do?type="+fatherKey,
				para : "id=" + $(this).attr("data-id"),
				title : "修改",
				btnOK : "修改"
			};
			addOrUpdate(para);//通用新增修改方法
		});
		//删除方法
		$('[data-opt=delete2]').die().live("click",
				function() {
					var id = $(this).attr("data-id");
					layer.confirm('确认删除所选择的吗?', function(index) {
						getJsonDataByPost("sysConfigInfo!delete.do",
								"dataIds=" + id, function(data) {
									if (data.result) {
										layer.msg("删除成功!");
										searchForm2();//查询
									} else {
										l.msg(data.msg);//错误消息弹出
									}
									layer.close(index);//关闭删除确认提示框
								}, true);
					});
				});
	</script>
</body>
</html>