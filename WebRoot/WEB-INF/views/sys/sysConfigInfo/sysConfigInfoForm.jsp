<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form form-content" action="${not empty sysConfigInfo?'sysConfigInfo!update.do':'sysConfigInfo!add.do'}"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="id" value="${sysConfigInfo.id}" />
    <input type="hidden"  name="type" value="${type}" />
<div class="layui-row">
	<div class="layui-form-item">
		<label class="layui-form-label">键</label>
		<div class="layui-input-inline">
		 <input type="text"   name="cKey"   placeholder="请输入键" value="${sysConfigInfo.CKey}" class="layui-input"  />
  		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label">值</label>
		<div class="layui-input-inline">
		 <input type="text"   name="cValue"   placeholder="请输入值" value="${sysConfigInfo.CValue}" class="layui-input"  />
  		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label">说明</label>
		<div class="layui-input-inline">
		 <input type="text"   name="note"   placeholder="请输入说明" value="${sysConfigInfo.note}" class="layui-input"  />
  		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label">排序</label>
		<div class="layui-input-inline">
		  <input type="text"   name="px"   lay-verify="number_" placeholder="请输入" value="${sysConfigInfo.px}" class="layui-input"  />
		</div>
	</div>
	</div>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="saveOrUpdate" lay-submit class="layui-hide" />
</form>
<script>
	form.render();//重新渲染表单
		layui.laydate.render({
		    elem: '#createTime' 
		    ,format: 'yyyy-MM-dd HH:mm:ss'
		   //,type:'datetime'
		 });
	form.on('submit(saveOrUpdate)', function(data){//表单提交方法
 		getJsonByajaxForm("saveOrUpdateForm","",function(data){
			if(data.result){
				layer.closeAll('page'); //关闭信息框
				layer.msg($('.layui-layer-btn0').text()+"成功!");
				searchForm2();//查询
			}else{
				l.msg(data.msg);//错误消息弹出
			}
		},true); 
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	}); 
</script>
