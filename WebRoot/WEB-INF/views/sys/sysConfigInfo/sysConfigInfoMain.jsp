<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="admin-main" >
	<!-- 查询条件 -->
	<form class="layui-elem-quote  layui-form layui-form-item" id="mainForm2"
		method="post">
		<!-- 查询条件 -->
		<div class="layui-inline">
			<div class="layui-input-inline">
				<input type="text" name="cKey" placeholder="键" value="${sysConfigInfo.CKey}"
					class="layui-input">
			</div>
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline">
				<input type="text" name="cValue" placeholder="值" value="${sysConfigInfo.CValue}"
					class="layui-input">
			</div>
		</div>
		<div class="layui-inline fr">
			<a href="javascript:;" class="layui-btn layui-btn-sm" 
				onclick="searchForm2();"> <i class="layui-icon">&#xe615;</i> 查询
			</a>
			<dyl:hasPermission kind="ADD" menuId="${menuId}">
				<a href="javascript:;" class="layui-btn layui-btn-sm" id="add2">
					<i class="layui-icon">&#xe608;</i> 添加
				</a>
			</dyl:hasPermission>
		</div>
	</form>
	<!-- 主table -->
		<div class="layui-form news_list">
			<table class="layui-table">
				<thead>
					<tr>
						<th>键</th>
						<th>值</th>
						<th>说明</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${configInfoList}" var="i">
						<tr>
							<td>${i.CKey}</td>
							<td>${i.CValue}</td>
							<td>${i.note}</td>
							<td><dyl:hasPermission kind="UPDATE" menuId="${menuId}">
									<a href="javascript:;" class="layui-btn layui-btn-xs"
										data-id="${i.id}" data-opt="edit2"> <i class="layui-icon">&#xe642;</i>
										修改
									</a>
								</dyl:hasPermission>
								<dyl:hasPermission kind="DELETE" menuId="${menuId}">
									<a href="javascript:;" class="layui-btn layui-btn-xs" data-id="${i.id}"
										data-opt="delete2">  删除
									</a>
								</dyl:hasPermission>
								</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
</div>
