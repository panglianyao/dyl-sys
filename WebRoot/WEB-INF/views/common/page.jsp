<%@include file="/taglib.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<c:set value="${not empty formID?formID:'form'}" var="formID" />
<%-- <form id="pageForm">
	<input type="hidden" value="${page.totalPage}" name="totalPage" />
	<input type="hidden" value="${page.currentPage}" name="currentPage" />
	<input type="hidden" name="currentPage" id="currentPage" value="1" />
</form> --%>
 <div class="waTabel">
	<div id="page" class="layui-table-page"></div>
  </div>
<script>
laypage.render({
    elem: 'page',
    limit: '${page.pageSize}',
    curr: '${page.currentPage}', //当前页
    layout: [ 'prev', 'page', 'next', 'skip','count', 'limit'],
    count: '${page.totalCount}', //数据总数
    prev: '<em><</em>',
    next: '<em>></em>',
    jump: function(obj, first){
    	$('#currentPage').val(obj.curr);
    	if(!first){ //一定要加此判断，否则初始时会无限刷新
	        $('#currentPage').val(obj.curr);
	        $('#search').click();
    	}
      }
  });
</script>