/**
 * 饼图通用
 * 
 * @param para
 */
function pieChart(para) {
	var pieChart = echarts.init(document.getElementById(para.chartId));
	var option = {
		tooltip : {
			trigger : 'item',
			formatter : "{a} <br/>{b} : {c} ({d}%)"
		},
		color : para.color,
		legend : {
			x : 'center',
			y : 'bottom',
			data : para.title,
			textStyle : {
				color : "#fff" // 文字颜色
			}
		},
		series : [ {
			name : para.name,
			type : 'pie',// 图标类型
			radius : [ 30, 110 ],
			center : [ '50%', '40%' ],// 图表定位
			roseType : para.roseType,// 图标风格
			data : para.data
		} ]
	};
	pieChart.setOption(option);
	pieChart.resize();
}
/**
 * 柱状图通用配置
 * 
 * @param para
 */
function barOrLineChart(para) {
	var chart = echarts.init(document.getElementById(para.chartId));
	// 指定图表的配置项和数据
	var option = {
		tooltip : {
			trigger : 'axis',
			axisPointer : { // 坐标轴指示器，坐标轴触发有效
				type : 'shadow' // 默认为直线，可选为：'line' | 'shadow'
			}
		},
		color : para.color,
		grid : {
			top : '8%',
			left : '3%',
			right : '4%',
			bottom : '15%',
			containLabel : true
		},
		legend : {
			data : para.title,
			textStyle : {
				color : "#fff" // 文字颜色
			}
		},
		xAxis : [ {
			type : 'category',
			data : para.xtitle,
			axisTick : {
				alignWithLabel : true
			},
			axisLabel : {
				rotate : 40,// 文字倾斜
				interval : 0,
				textStyle : {
					color : '#fff'
				}
			},
			axisLine : {
				lineStyle : {
					type : 'solid',
					color : '#fff',// 左边线的颜色
					width : '.5'// 坐标线的宽度
				}
			},
		} ],
		yAxis : [ {
			type : 'value',
			splitLine : {
				show : false
			},// 去除网格线
			axisLine : {
				lineStyle : {
					type : 'solid',
					color : '#fff',// 左边线的颜色
					width : '.5'// 坐标线的宽度
				}
			},
			axisLabel : {
				textStyle : {
					color : '#fff'
				}
			}
		} ],
		dataZoom : [
				{
					type : 'slider',
					show : true,
					xAxisIndex : [ 0 ],
					handleSize : 20,// 滑动条的 左右2个滑动条的大小
					height : 8,// 组件高度
					left : 30, // 左边的距离
					right : 40,// 右边的距离
					bottom : 30,// 右边的距离
					handleColor : '#ddd',// h滑动图标的颜色
					handleStyle : {
						borderColor : "#cacaca",
						borderWidth : "1",
						shadowBlur : 2,
						background : "#ddd",
						shadowColor : "#ddd",
					},
					fillerColor : new echarts.graphic.LinearGradient(1, 0, 0,
							0, [ {
								// 给颜色设置渐变色 前面4个参数，给第一个设置1，第四个设置0 ，就是水平渐变
								// 给第一个设置0，第四个设置1，就是垂直渐变
								offset : 0,
								color : '#1eb5e5'
							}, {
								offset : 1,
								color : '#5ccbb1'
							} ]),
					backgroundColor : '#ddd',// 两边未选中的滑动条区域的颜色
					showDataShadow : false,// 是否显示数据阴影 默认auto
					showDetail : false,// 即拖拽时候是否显示详细数值信息 默认true
					handleIcon : 'M-292,322.2c-3.2,0-6.4-0.6-9.3-1.9c-2.9-1.2-5.4-2.9-7.6-5.1s-3.9-4.8-5.1-7.6c-1.3-3-1.9-6.1-1.9-9.3c0-3.2,0.6-6.4,1.9-9.3c1.2-2.9,2.9-5.4,5.1-7.6s4.8-3.9,7.6-5.1c3-1.3,6.1-1.9,9.3-1.9c3.2,0,6.4,0.6,9.3,1.9c2.9,1.2,5.4,2.9,7.6,5.1s3.9,4.8,5.1,7.6c1.3,3,1.9,6.1,1.9,9.3c0,3.2-0.6,6.4-1.9,9.3c-1.2,2.9-2.9,5.4-5.1,7.6s-4.8,3.9-7.6,5.1C-285.6,321.5-288.8,322.2-292,322.2z',
					filterMode : 'filter',
				},
				// 下面这个属性是里面拖到
				{
					type : 'inside',
					show : true,
					xAxisIndex : [ 0 ],
					start : 1,
					end : 100
				} ],
		series : para.series
	};
	chart.setOption(option);
	chart.resize();
}
