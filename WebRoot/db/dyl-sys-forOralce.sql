﻿prompt PL/SQL Developer import file
prompt Created on 2017年11月9日 by Administrator
set feedback off
set define off
prompt Creating SYS_AUTH_ROLE...
create table SYS_AUTH_ROLE
(
  id          NUMBER,
  role_id     NUMBER,
  menu_id     NUMBER,
  kind_id     NUMBER,
  create_time DATE default sysdate
)
;
comment on column SYS_AUTH_ROLE.role_id
  is '角色id';
comment on column SYS_AUTH_ROLE.menu_id
  is '菜单id';
comment on column SYS_AUTH_ROLE.kind_id
  is '权限id';
comment on column SYS_AUTH_ROLE.create_time
  is '创建时间';

prompt Creating SYS_CONFIG...
create table SYS_CONFIG
(
  id       NUMBER,
  c_key    VARCHAR2(64),
  c_value  VARCHAR2(64),
  note     VARCHAR2(128),
  px       NUMBER,
  can_edit CHAR(1) default 0
)
;
comment on column SYS_CONFIG.c_key
  is '键';
comment on column SYS_CONFIG.c_value
  is '值';
comment on column SYS_CONFIG.note
  is '说明';
comment on column SYS_CONFIG.px
  is '排序';
comment on column SYS_CONFIG.can_edit
  is '是否可以修改';

prompt Creating SYS_CONFIG_INFO...
create table SYS_CONFIG_INFO
(
  id          NUMBER,
  type        VARCHAR2(256),
  c_key       VARCHAR2(256),
  c_value     VARCHAR2(256),
  create_time DATE default sysdate,
  note        VARCHAR2(256),
  px          NUMBER
)
;
comment on table SYS_CONFIG_INFO
  is '系统配置表';
comment on column SYS_CONFIG_INFO.c_key
  is '键';
comment on column SYS_CONFIG_INFO.c_value
  is '值';
comment on column SYS_CONFIG_INFO.note
  is '说明';

prompt Creating SYS_FILEINFO...
create table SYS_FILEINFO
(
  id          NUMBER,
  real_name   VARCHAR2(100),
  upload_name VARCHAR2(100),
  file_size   NUMBER,
  file_url    VARCHAR2(200),
  creator     NUMBER,
  create_time DATE default sysdate,
  c_id        NUMBER
)
;
comment on column SYS_FILEINFO.real_name
  is '文件存储的真实名称';
comment on column SYS_FILEINFO.upload_name
  is '上传文件名称';
comment on column SYS_FILEINFO.file_size
  is '文件大小';
comment on column SYS_FILEINFO.file_url
  is '文件下载地址';
comment on column SYS_FILEINFO.creator
  is '上传人';
comment on column SYS_FILEINFO.create_time
  is '上传时间';
comment on column SYS_FILEINFO.c_id
  is '关联的业务id';

prompt Creating SYS_KIND...
create table SYS_KIND
(
  id   NUMBER,
  name VARCHAR2(20)
)
;
comment on column SYS_KIND.id
  is '种类ID';
comment on column SYS_KIND.name
  is '种类名称';

prompt Creating SYS_LOG...
create table SYS_LOG
(
  id        NUMBER(20),
  url       VARCHAR2(255),
  para      VARCHAR2(555),
  loginuser NUMBER(20),
  ip        VARCHAR2(255),
  time      DATE
)
;

prompt Creating SYS_MENU...
create table SYS_MENU
(
  id           NUMBER not null,
  pid          NUMBER,
  oid          NUMBER,
  title        VARCHAR2(50),
  note         VARCHAR2(100),
  state        CHAR(1) default 0,
  url          VARCHAR2(100),
  icon         VARCHAR2(100),
  create_time  DATE default sysdate,
  creator      NUMBER,
  action_class VARCHAR2(50),
  view_level   NUMBER default 5
)
;
comment on column SYS_MENU.pid
  is '父Id';
comment on column SYS_MENU.oid
  is '排序';
comment on column SYS_MENU.title
  is '标题';
comment on column SYS_MENU.note
  is '备注';
comment on column SYS_MENU.state
  is '状态,0正常，1禁用';
comment on column SYS_MENU.url
  is '链接';
comment on column SYS_MENU.icon
  is '图标';
comment on column SYS_MENU.create_time
  is '创建时间';
comment on column SYS_MENU.creator
  is '创建者';
comment on column SYS_MENU.action_class
  is '对应的类';
comment on column SYS_MENU.view_level
  is '等级';
alter table SYS_MENU
  add primary key (ID);

prompt Creating SYS_MENU_KIND...
create table SYS_MENU_KIND
(
  id          NUMBER,
  menu_id     NUMBER,
  kind_id     NUMBER,
  create_time DATE default sysdate
)
;
comment on column SYS_MENU_KIND.menu_id
  is '菜单ID';
comment on column SYS_MENU_KIND.kind_id
  is '权限ID';
comment on column SYS_MENU_KIND.create_time
  is '创建时间';

prompt Creating SYS_QUARTZ...
create table SYS_QUARTZ
(
  id             NUMBER,
  triggername    VARCHAR2(40),
  cronexpression VARCHAR2(40),
  jobdetailname  VARCHAR2(40),
  targetobject   VARCHAR2(40),
  methodname     VARCHAR2(40),
  concurrent     NUMBER default 0,
  state          NUMBER default 0,
  create_date    DATE default sysdate
)
;
comment on column SYS_QUARTZ.triggername
  is '触发器名称';
comment on column SYS_QUARTZ.cronexpression
  is '时间表达式';
comment on column SYS_QUARTZ.jobdetailname
  is '脚本名称';
comment on column SYS_QUARTZ.targetobject
  is '目标类';
comment on column SYS_QUARTZ.methodname
  is '方法名';
comment on column SYS_QUARTZ.concurrent
  is '是否并发启动任务';
comment on column SYS_QUARTZ.state
  is '0:关闭,1启用';

prompt Creating SYS_ROLE...
create table SYS_ROLE
(
  id          NUMBER,
  name        VARCHAR2(200),
  note        VARCHAR2(200),
  creator     NUMBER,
  create_time DATE default sysdate
)
;
comment on column SYS_ROLE.id
  is '主键';
comment on column SYS_ROLE.name
  is '角色名称';
comment on column SYS_ROLE.note
  is '备注';
comment on column SYS_ROLE.creator
  is '创建者Id';
comment on column SYS_ROLE.create_time
  is '创建时间';

prompt Creating SYS_ROLE_USER...
create table SYS_ROLE_USER
(
  id          NUMBER not null,
  roleid      NUMBER,
  userid      NUMBER,
  create_time DATE default sysdate,
  creator     NUMBER
)
;
comment on column SYS_ROLE_USER.id
  is '主键';
comment on column SYS_ROLE_USER.roleid
  is '角色id';
comment on column SYS_ROLE_USER.userid
  is '用户id';
comment on column SYS_ROLE_USER.create_time
  is '创建时间';
comment on column SYS_ROLE_USER.creator
  is '创建人';
alter table SYS_ROLE_USER
  add constraint ID primary key (ID);

prompt Creating SYS_USER...
create table SYS_USER
(
  id          NUMBER not null,
  username    VARCHAR2(50) not null,
  name        VARCHAR2(50),
  password    VARCHAR2(100),
  email       VARCHAR2(100),
  phone       VARCHAR2(100),
  state       CHAR(1) default 0,
  create_time DATE default sysdate,
  creator     NUMBER
)
;
comment on column SYS_USER.username
  is '用户名';
comment on column SYS_USER.name
  is '姓名';
comment on column SYS_USER.password
  is '密码';
comment on column SYS_USER.email
  is '邮箱';
comment on column SYS_USER.phone
  is '电话';
comment on column SYS_USER.state
  is '状态0:正常,1禁用';
comment on column SYS_USER.create_time
  is '创建时间';
comment on column SYS_USER.creator
  is '创建者';

prompt Loading SYS_AUTH_ROLE...
insert into SYS_AUTH_ROLE (id, role_id, menu_id, kind_id, create_time)
values (100024, 2, 2, 1, to_date('26-05-2017 13:44:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_AUTH_ROLE (id, role_id, menu_id, kind_id, create_time)
values (100025, 2, 2, 4, to_date('26-05-2017 13:44:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_AUTH_ROLE (id, role_id, menu_id, kind_id, create_time)
values (100026, 2, 3, 1, to_date('26-05-2017 13:44:32', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 3 records loaded
prompt Loading SYS_CONFIG...
insert into SYS_CONFIG (id, c_key, c_value, note, px, can_edit)
values (1, 'styjgz', '身体预警规则', null, 1, '0');
insert into SYS_CONFIG (id, c_key, c_value, note, px, can_edit)
values (2, 'xlyjgz', '心理预警规则', null, null, '0');
commit;
prompt 2 records loaded
prompt Loading SYS_CONFIG_INFO...
insert into SYS_CONFIG_INFO (id, type, c_key, c_value, create_time, note, px)
values (1, 'styjgz', '身体预警规则1', '身体预警规则1', to_date('01-11-2017 15:34:58', 'dd-mm-yyyy hh24:mi:ss'), null, null);
insert into SYS_CONFIG_INFO (id, type, c_key, c_value, create_time, note, px)
values (2, 'styjgz', '身体预警规则2', '身体预警规则2', to_date('01-11-2017 15:34:58', 'dd-mm-yyyy hh24:mi:ss'), null, null);
insert into SYS_CONFIG_INFO (id, type, c_key, c_value, create_time, note, px)
values (3, 'styjgz', '身体预警规则3', '身体预警规则3', to_date('01-11-2017 15:34:58', 'dd-mm-yyyy hh24:mi:ss'), null, null);
insert into SYS_CONFIG_INFO (id, type, c_key, c_value, create_time, note, px)
values (4, 'xlyjgz', '心理预警规则1', '心理预警规则1', to_date('01-11-2017 15:34:58', 'dd-mm-yyyy hh24:mi:ss'), null, null);
insert into SYS_CONFIG_INFO (id, type, c_key, c_value, create_time, note, px)
values (5, 'xlyjgz', '心理预警规则2', '心理预警规则2', to_date('01-11-2017 15:34:58', 'dd-mm-yyyy hh24:mi:ss'), null, null);
insert into SYS_CONFIG_INFO (id, type, c_key, c_value, create_time, note, px)
values (6, 'xlyjgz', '心理预警规则3', '心理预警规则3', to_date('01-11-2017 15:34:58', 'dd-mm-yyyy hh24:mi:ss'), null, null);
insert into SYS_CONFIG_INFO (id, type, c_key, c_value, create_time, note, px)
values (7, 'xlyjgz', '心理预警规则4', '心理预警规则4', to_date('01-11-2017 15:34:59', 'dd-mm-yyyy hh24:mi:ss'), null, null);
commit;
prompt 7 records loaded
prompt Loading SYS_FILEINFO...
insert into SYS_FILEINFO (id, real_name, upload_name, file_size, file_url, creator, create_time, c_id)
values (100397, '201706221547308.docx', 'zhuanhuan.docx', 44889, 'download.do?fileName=201706221547308.docx&name=zhuanhuan.docx', 0, to_date('22-06-2017 15:29:13', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_FILEINFO (id, real_name, upload_name, file_size, file_url, creator, create_time, c_id)
values (100395, '201706221547108.doc', '测试.doc', 114688, 'download.do?fileName=201706221547108.doc&name=测试.doc', 0, to_date('22-06-2017 15:29:13', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_FILEINFO (id, real_name, upload_name, file_size, file_url, creator, create_time, c_id)
values (100396, '201706221547926.docx', '论文.docx', 158592, 'download.do?fileName=201706221547926.docx&name=论文.docx', 0, to_date('22-06-2017 15:29:13', 'dd-mm-yyyy hh24:mi:ss'), null);
commit;
prompt 3 records loaded
prompt Loading SYS_KIND...
insert into SYS_KIND (id, name)
values (1, '查看');
insert into SYS_KIND (id, name)
values (2, '新增');
insert into SYS_KIND (id, name)
values (3, '修改');
insert into SYS_KIND (id, name)
values (4, '删除');
commit;
prompt 4 records loaded
prompt Loading SYS_LOG...
prompt Table is empty
prompt Loading SYS_MENU...
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (8, 1, 10, '日志管理', null, '0', 'sysLog!main.do', '&#xe621;', to_date('01-06-2017 15:29:40', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (1, 0, 1, '系统管理', null, '0', null, '&#xe614;', to_date('15-03-2017 15:42:06', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 2);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (2, 1, -1, '用户管理', null, '0', 'sysUser!main.do', '&#xe612;', to_date('15-03-2017 15:43:57', 'dd-mm-yyyy hh24:mi:ss'), 0, 'sysUserAction', 2);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (3, 1, 0, '菜单管理', null, '0', 'sysMenu!main.do', '&#xe62a;', to_date('15-03-2017 15:43:57', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (4, 1, 6, '角色管理', null, '0', 'sysRole!main.do', '&#xe61b;', to_date('05-04-2017 15:46:44', 'dd-mm-yyyy hh24:mi:ss'), 0, 'sysRoleAction', 2);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (9, 1, 11, '通用上传', null, '0', 'sysFileinfo!main.do', '&#xe62f;', to_date('06-06-2017 15:11:38', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (6, 1, 7, '数据库监控', null, '0', 'druid/index.html', '&#xe636;', to_date('24-05-2017 16:13:56', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (5, 1, 9, '代码生成器', null, '0', 'easyCode!main.do', '&#xe62c;', to_date('31-05-2017 14:55:18', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (7, 1, 8, '定时任务管理', null, '0', 'sysQuartz!main.do', '&#xe60a;', to_date('25-05-2017 16:00:25', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (130353, 1, 12, '值列表', null, '0', 'sysConfig!main.do', '&#xe62d;', to_date('09-11-2017 18:25:45', 'dd-mm-yyyy hh24:mi:ss'), 0, 'sysConfigAction', 5);
commit;
prompt 10 records loaded
prompt Loading SYS_MENU_KIND...
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (206, 87, 1, to_date('20-04-2017 14:11:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (207, 87, 2, to_date('20-04-2017 14:11:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (192, 4, 1, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (193, 4, 2, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (186, 2, 1, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (187, 2, 2, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (188, 2, 3, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (189, 2, 4, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (194, 4, 3, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (195, 4, 4, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (190, 3, 1, to_date('20-04-2017 10:12:20', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (191, 3, 2, to_date('20-04-2017 10:12:20', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (130358, 130353, 1, to_date('09-11-2017 18:27:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (130359, 130353, 2, to_date('09-11-2017 18:27:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (130360, 130353, 3, to_date('09-11-2017 18:27:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (130361, 130353, 4, to_date('09-11-2017 18:27:36', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 16 records loaded
prompt Loading SYS_QUARTZ...
insert into SYS_QUARTZ (id, triggername, cronexpression, jobdetailname, targetobject, methodname, concurrent, state, create_date)
values (1, '测试1', '0/3 * * * * ?', 'detailname', 'dyl.sys.quartz.TestQuarz', 'haha', 1, 0, to_date('25-05-2017 16:02:56', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_QUARTZ (id, triggername, cronexpression, jobdetailname, targetobject, methodname, concurrent, state, create_date)
values (100032, '系统访问日志定时器', '0/30 * * * * ?', 'logInfo', 'logQuarz', 'execute', 1, 0, to_date('06-06-2017 10:26:08', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 2 records loaded
prompt Loading SYS_ROLE...
insert into SYS_ROLE (id, name, note, creator, create_time)
values (1, '超级管理员', '超级管理员', 1, to_date('06-04-2017 14:11:49', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_ROLE (id, name, note, creator, create_time)
values (2, '测试角色', '测试角色', 1, to_date('20-04-2017 14:11:44', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_ROLE (id, name, note, creator, create_time)
values (0, '开发管理员', '开发管理员最高权限', 1, to_date('20-04-2017 14:26:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_ROLE (id, name, note, creator, create_time)
values (212, '测试2', '测试21', 2, to_date('20-04-2017 16:34:17', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 4 records loaded
prompt Loading SYS_ROLE_USER...
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (209, 0, 0, to_date('20-04-2017 16:33:47', 'dd-mm-yyyy hh24:mi:ss'), 2);
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (210, 1, 2, to_date('20-04-2017 16:33:54', 'dd-mm-yyyy hh24:mi:ss'), 2);
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (100028, 2, 211, to_date('27-05-2017 13:44:30', 'dd-mm-yyyy hh24:mi:ss'), 0);
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (100029, 212, 211, to_date('27-05-2017 13:44:30', 'dd-mm-yyyy hh24:mi:ss'), 0);
commit;
prompt 4 records loaded
prompt Loading SYS_USER...
insert into SYS_USER (id, username, name, password, email, phone, state, create_time, creator)
values (0, 'dev', 'dev', '202CB962AC59075B964B07152D234B70', '大神', null, '0', to_date('23-03-2017 20:26:26', 'dd-mm-yyyy hh24:mi:ss'), 0);
insert into SYS_USER (id, username, name, password, email, phone, state, create_time, creator)
values (211, 'test', '测试人员1', '098F6BCD4621D373CADE4E832627B4F6', null, '111', '0', to_date('20-04-2017 16:34:06', 'dd-mm-yyyy hh24:mi:ss'), 0);
insert into SYS_USER (id, username, name, password, email, phone, state, create_time, creator)
values (2, 'admin', 'admin', '21232F297A57A5A743894A0E4A801FC3', null, null, '0', to_date('23-03-2017 20:26:41', 'dd-mm-yyyy hh24:mi:ss'), 0);
commit;
prompt 3 records loaded
set feedback on
set define on
prompt Done.
