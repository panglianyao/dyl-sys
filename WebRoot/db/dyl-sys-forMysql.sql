/*
Navicat MySQL Data Transfer

Source Server         : 测试57
Source Server Version : 50077
Source Host           : 10.5.110.57:3306
Source Database       : ts-admin

Target Server Type    : MYSQL
Target Server Version : 50077
File Encoding         : 65001

Date: 2018-01-11 17:00:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for SYS_AUTH_ROLE
-- ----------------------------
DROP TABLE IF EXISTS `SYS_AUTH_ROLE`;
CREATE TABLE `SYS_AUTH_ROLE` (
  `id` decimal(10,0) NOT NULL COMMENT '角色id',
  `role_id` decimal(10,0) default NULL COMMENT ' 角色id',
  `menu_id` decimal(10,0) default NULL COMMENT '菜单id',
  `kind_id` decimal(10,0) default NULL COMMENT '权限id',
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_AUTH_ROLE
-- ----------------------------
INSERT INTO `SYS_AUTH_ROLE` VALUES ('110220', '2', '2', '4', '2017-06-16 16:42:33');
INSERT INTO `SYS_AUTH_ROLE` VALUES ('110219', '2', '2', '2', '2017-06-16 16:42:33');
INSERT INTO `SYS_AUTH_ROLE` VALUES ('110218', '2', '2', '1', '2017-06-16 16:42:33');

-- ----------------------------
-- Table structure for SYS_CONFIG
-- ----------------------------
DROP TABLE IF EXISTS `SYS_CONFIG`;
CREATE TABLE `SYS_CONFIG` (
  `id` int(11) default NULL,
  `c_key` varchar(64) default NULL,
  `c_value` varchar(64) default NULL,
  `note` varchar(128) default NULL,
  `px` int(11) default NULL,
  `can_edit` char(1) default '0',
  `type` varchar(64) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_CONFIG
-- ----------------------------
INSERT INTO `SYS_CONFIG` VALUES ('112848', 'test', 'test2', 'test', '1', null, null);
INSERT INTO `SYS_CONFIG` VALUES ('112853', 'aaa', 'aaa', 'aaa', null, null, null);

-- ----------------------------
-- Table structure for SYS_CONFIG_INFO
-- ----------------------------
DROP TABLE IF EXISTS `SYS_CONFIG_INFO`;
CREATE TABLE `SYS_CONFIG_INFO` (
  `id` int(11) default NULL,
  `type` varchar(256) default NULL,
  `c_key` varchar(256) default NULL,
  `c_value` varchar(256) default NULL,
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `note` varchar(256) default NULL,
  `px` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_CONFIG_INFO
-- ----------------------------
INSERT INTO `SYS_CONFIG_INFO` VALUES ('112854', 'aaa', 'a1', 'a2', '2018-01-11 10:19:53', 'a3', null);
INSERT INTO `SYS_CONFIG_INFO` VALUES ('112855', 'test', 'b1', 'b2', '2018-01-11 10:20:03', 'b3', null);

-- ----------------------------
-- Table structure for SYS_FILEINFO
-- ----------------------------
DROP TABLE IF EXISTS `SYS_FILEINFO`;
CREATE TABLE `SYS_FILEINFO` (
  `id` int(11) default NULL,
  `real_name` varchar(100) default NULL COMMENT '文件存储的真实名称',
  `upload_name` varchar(100) default NULL COMMENT '上传文件名称',
  `file_size` decimal(10,0) default NULL COMMENT '文件大小',
  `file_url` varchar(200) default NULL COMMENT '文件下载地址',
  `creator` decimal(10,0) default NULL COMMENT '上传人',
  `create_time` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '上传时间',
  `c_id` int(11) default NULL COMMENT '关联的业务id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_FILEINFO
-- ----------------------------
INSERT INTO `SYS_FILEINFO` VALUES ('110312', '201706221607951.docx', 'zhuanhuan.docx', '44889', 'download.do?fileName=201706221607951.docx&name=zhuanhuan.docx', '0', null, null);
INSERT INTO `SYS_FILEINFO` VALUES ('110313', '201706221607464.doc', 'zhuanhuan.doc', '12128', 'download.do?fileName=201706221607464.doc&name=zhuanhuan.doc', '0', null, null);
INSERT INTO `SYS_FILEINFO` VALUES ('110320', '201706221609549.docx', 'zhuanhuan.docx', '44889', 'download.do?fileName=201706221609549.docx&name=zhuanhuan.docx', '0', '2017-06-22 16:09:48', null);

-- ----------------------------
-- Table structure for SYS_KIND
-- ----------------------------
DROP TABLE IF EXISTS `SYS_KIND`;
CREATE TABLE `SYS_KIND` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(20) default NULL COMMENT '种类名称',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_KIND
-- ----------------------------
INSERT INTO `SYS_KIND` VALUES ('1', '查看');
INSERT INTO `SYS_KIND` VALUES ('2', '新增');
INSERT INTO `SYS_KIND` VALUES ('3', '修改');
INSERT INTO `SYS_KIND` VALUES ('4', '删除');

-- ----------------------------
-- Table structure for SYS_LOG
-- ----------------------------
DROP TABLE IF EXISTS `SYS_LOG`;
CREATE TABLE `SYS_LOG` (
  `id` bigint(20) default NULL,
  `url` varchar(255) default NULL,
  `para` varchar(255) default NULL,
  `loginUser` bigint(20) default NULL COMMENT '登陆用户',
  `ip` varchar(255) default NULL COMMENT 'Ip地址',
  `time` timestamp NULL default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for SYS_MENU
-- ----------------------------
DROP TABLE IF EXISTS `SYS_MENU`;
CREATE TABLE `SYS_MENU` (
  `id` decimal(10,0) NOT NULL,
  `pid` decimal(10,0) default NULL COMMENT '父Id',
  `oid` decimal(10,0) default NULL COMMENT '排序',
  `title` varchar(50) default NULL COMMENT '名称',
  `note` varchar(100) default NULL COMMENT '说明',
  `state` char(1) default '0' COMMENT '状态',
  `url` varchar(100) default NULL COMMENT '地址',
  `icon` varchar(100) default NULL COMMENT '图片代码',
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `creator` decimal(10,0) default NULL,
  `action_class` varchar(50) default NULL,
  `view_level` decimal(10,0) default '5' COMMENT '查看等级',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_MENU
-- ----------------------------
INSERT INTO `SYS_MENU` VALUES ('1', '0', '1', '系统管理', '', '0', '', '&#xe614;', '2018-01-10 18:19:55', '0', '', '2');
INSERT INTO `SYS_MENU` VALUES ('2', '1', '-2', '用户管理', '', '0', 'sysUser!main.do', '&#xe612;', '2018-01-11 15:01:05', '0', 'sysUserAction', '2');
INSERT INTO `SYS_MENU` VALUES ('3', '1', '8', '菜单管理', '', '0', 'sysMenu!main.do', '&#xe60e;', '2018-01-11 09:55:37', '0', '', '1');
INSERT INTO `SYS_MENU` VALUES ('4', '1', '5', '角色管理', null, '0', 'sysRole!main.do', '&#xe61b;', '2018-01-11 15:01:05', null, 'sysRoleAction', '2');
INSERT INTO `SYS_MENU` VALUES ('6', '1', '7', '数据库监控', '', '0', 'druid/index.html', '&#xe636;', '2017-06-01 15:16:57', '0', '', '1');
INSERT INTO `SYS_MENU` VALUES ('7', '1', '9', '定时任务管理', '', '0', 'sysQuartz!main.do', '&#xe62c;', '2017-06-16 16:40:08', '0', '', '1');
INSERT INTO `SYS_MENU` VALUES ('5', '1', '10', '代码生成器', '', '0', 'easyCode!main.do', '&#xe60a;', '2017-06-16 16:40:08', '0', '', '1');
INSERT INTO `SYS_MENU` VALUES ('8', '1', '11', '日志管理', '', '0', 'sysLog!main.do', '&#xe61d;', '2017-09-28 14:50:38', '0', '', '1');
INSERT INTO `SYS_MENU` VALUES ('112858', '1', '6', '系统监控', '', '0', 'monitoring', '&#xe643;', '2018-01-11 15:01:05', '0', '', '5');
INSERT INTO `SYS_MENU` VALUES ('112850', '1', '15', '缓存管理', '', '0', 'cache!main.do', '&#xe60e;', '2018-01-11 09:55:46', '0', '', '5');
INSERT INTO `SYS_MENU` VALUES ('112847', '1', '14', '系统配置管理', '', '0', 'sysConfig!main.do', '&#xe62d;', '2017-09-28 14:50:19', '0', '', '5');

-- ----------------------------
-- Table structure for SYS_MENU_KIND
-- ----------------------------
DROP TABLE IF EXISTS `SYS_MENU_KIND`;
CREATE TABLE `SYS_MENU_KIND` (
  `id` decimal(10,0) NOT NULL,
  `menu_id` decimal(10,0) default NULL COMMENT '菜单Id',
  `kind_id` decimal(10,0) default NULL COMMENT '种类Id',
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_MENU_KIND
-- ----------------------------
INSERT INTO `SYS_MENU_KIND` VALUES ('206', '87', '1', '2017-05-27 11:25:37');
INSERT INTO `SYS_MENU_KIND` VALUES ('207', '87', '2', '2017-05-27 11:25:37');
INSERT INTO `SYS_MENU_KIND` VALUES ('192', '4', '1', '2017-05-27 11:25:37');
INSERT INTO `SYS_MENU_KIND` VALUES ('193', '4', '2', '2017-05-27 11:25:37');
INSERT INTO `SYS_MENU_KIND` VALUES ('100064', '2', '4', '2017-06-01 15:41:26');
INSERT INTO `SYS_MENU_KIND` VALUES ('100063', '2', '3', '2017-06-01 15:41:26');
INSERT INTO `SYS_MENU_KIND` VALUES ('100062', '2', '2', '2017-06-01 15:41:26');
INSERT INTO `SYS_MENU_KIND` VALUES ('100061', '2', '1', '2017-06-01 15:41:26');
INSERT INTO `SYS_MENU_KIND` VALUES ('194', '4', '3', '2017-05-27 11:25:38');
INSERT INTO `SYS_MENU_KIND` VALUES ('195', '4', '4', '2017-05-27 11:25:38');
INSERT INTO `SYS_MENU_KIND` VALUES ('112852', '3', '2', '2018-01-11 09:55:37');
INSERT INTO `SYS_MENU_KIND` VALUES ('112851', '3', '1', '2018-01-11 09:55:37');
INSERT INTO `SYS_MENU_KIND` VALUES ('100057', '100053', '2', '2017-05-27 16:27:29');
INSERT INTO `SYS_MENU_KIND` VALUES ('100056', '100053', '1', '2017-05-27 16:27:29');
INSERT INTO `SYS_MENU_KIND` VALUES ('109903', '109647', '4', '2017-06-07 15:59:42');
INSERT INTO `SYS_MENU_KIND` VALUES ('109902', '109647', '3', '2017-06-07 15:59:42');
INSERT INTO `SYS_MENU_KIND` VALUES ('109901', '109647', '2', '2017-06-07 15:59:42');
INSERT INTO `SYS_MENU_KIND` VALUES ('109900', '109647', '1', '2017-06-07 15:59:42');

-- ----------------------------
-- Table structure for SYS_QUARTZ
-- ----------------------------
DROP TABLE IF EXISTS `SYS_QUARTZ`;
CREATE TABLE `SYS_QUARTZ` (
  `id` decimal(10,0) NOT NULL,
  `triggername` varchar(40) default NULL COMMENT '触发器名称',
  `cronexpression` varchar(40) default NULL COMMENT '时间表达式',
  `jobdetailname` varchar(40) default NULL COMMENT '任务名称',
  `targetobject` varchar(40) default NULL COMMENT '目标名称',
  `methodname` varchar(40) default NULL COMMENT '方法名称',
  `concurrent` decimal(10,0) default '0' COMMENT '是否并发',
  `state` decimal(10,0) default '0' COMMENT '状态',
  `create_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_QUARTZ
-- ----------------------------
INSERT INTO `SYS_QUARTZ` VALUES ('1', '测试', '0/3 * * * * ?', 'detailname', 'testQuarz', 'haha', '1', '0', '2017-06-16 16:49:24');
INSERT INTO `SYS_QUARTZ` VALUES ('100069', '系统访问日志定时器', '0/30 * * * * ?', 'logInfo', 'logQuarz', 'execute', '1', '0', '2017-09-28 14:51:58');

-- ----------------------------
-- Table structure for SYS_ROLE
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE`;
CREATE TABLE `SYS_ROLE` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(200) default NULL COMMENT '名称',
  `note` varchar(200) default NULL COMMENT '说明',
  `creator` decimal(10,0) default NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_ROLE
-- ----------------------------
INSERT INTO `SYS_ROLE` VALUES ('1', '超级管理员', '超级管理员', '1', '2017-05-27 11:26:28');
INSERT INTO `SYS_ROLE` VALUES ('2', '测试33', '111', '1', '2018-01-11 16:41:53');
INSERT INTO `SYS_ROLE` VALUES ('0', '开发管理员', '开发管理员最高权限', '1', '2017-05-27 11:26:28');
INSERT INTO `SYS_ROLE` VALUES ('212', '测试2', '测试21', '2', '2017-05-27 11:26:28');

-- ----------------------------
-- Table structure for SYS_ROLE_USER
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE_USER`;
CREATE TABLE `SYS_ROLE_USER` (
  `id` decimal(10,0) NOT NULL,
  `roleid` decimal(10,0) default NULL COMMENT '角色id',
  `userid` decimal(10,0) default NULL COMMENT '用户id',
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `creator` decimal(10,0) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_ROLE_USER
-- ----------------------------
INSERT INTO `SYS_ROLE_USER` VALUES ('209', '0', '0', '2017-05-27 11:26:47', '2');
INSERT INTO `SYS_ROLE_USER` VALUES ('210', '1', '2', '2017-05-27 11:26:47', '2');
INSERT INTO `SYS_ROLE_USER` VALUES ('262', '2', '211', '2017-05-27 11:26:47', '211');
INSERT INTO `SYS_ROLE_USER` VALUES ('263', '212', '211', '2017-05-27 11:26:47', '211');

-- ----------------------------
-- Table structure for SYS_SEQUENCE
-- ----------------------------
DROP TABLE IF EXISTS `SYS_SEQUENCE`;
CREATE TABLE `SYS_SEQUENCE` (
  `name` varchar(50) NOT NULL COMMENT '序列的名字，唯一',
  `current_value` bigint(20) NOT NULL COMMENT '当前的值',
  `increment_value` int(11) NOT NULL default '1' COMMENT '步长，默认为1',
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_SEQUENCE
-- ----------------------------
INSERT INTO `SYS_SEQUENCE` VALUES ('seq_id', '112861', '1');

-- ----------------------------
-- Table structure for SYS_USER
-- ----------------------------
DROP TABLE IF EXISTS `SYS_USER`;
CREATE TABLE `SYS_USER` (
  `id` decimal(10,0) NOT NULL default '0',
  `username` varchar(50) NOT NULL,
  `name` varchar(50) default NULL COMMENT '姓名',
  `password` varchar(100) default NULL COMMENT '密码',
  `email` varchar(100) default NULL COMMENT '邮箱',
  `phone` varchar(100) default NULL COMMENT '电话',
  `state` varchar(1) default '0' COMMENT '状态',
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `creator` decimal(10,0) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_USER
-- ----------------------------
INSERT INTO `SYS_USER` VALUES ('0', 'dev', 'dev', '202CB962AC59075B964B07152D234B70', '大神', null, '0', '2017-05-27 11:27:09', '0');
INSERT INTO `SYS_USER` VALUES ('211', 'test', '测试人员1', '098F6BCD4621D373CADE4E832627B4F6', null, '222', '0', '2017-05-27 11:27:09', '0');
INSERT INTO `SYS_USER` VALUES ('2', 'admin', 'admin', '21232F297A57A5A743894A0E4A801FC3', null, null, '0', '2017-05-27 16:32:53', '0');
INSERT INTO `SYS_USER` VALUES ('100044', 'test2', '测试2', 'E10ADC3949BA59ABBE56E057F20F883E', 'sdfsdf@sdf', '123123', '0', '2018-01-11 16:41:46', '0');

-- ----------------------------
-- Function structure for func_currval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_currval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_currval`(seq_name varchar(50)) RETURNS int(11)
begin
 declare value integer;
 set value = 0;
 select current_value into value
 from SYS_SEQUENCE
 where name = seq_name;
 return value;
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for func_nextval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_nextval`(seq_name varchar(50)) RETURNS int(11)
begin
 update SYS_SEQUENCE
 set current_value = current_value + increment_value
 where name = seq_name;
 return func_currval(seq_name);
end
;;
DELIMITER ;
